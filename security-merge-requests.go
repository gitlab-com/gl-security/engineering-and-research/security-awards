package main

import (
	"fmt"
	"io/ioutil"
	"time"

	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/gitlab"
	"sigs.k8s.io/yaml"
)

const monthsBack = 3 // Fetch MRs created in the last x months

func listSecurityMRsCmd(git gitlab.Client) func(*cli.Context) error {
	return func(c *cli.Context) error {

		securityMRs, err := git.SecurityMRs().ListMergeRequests(time.Now().AddDate(0, -monthsBack, 0))
		if err != nil {
			return err
		}

		// Load the awards to compare with the list
		awardsList, err := awards.LoadAwards(c.String(dataFileFlagName))
		if err != nil {
			return err
		}

		newAwards := awards.Awards{}

		for i := range securityMRs {
			smr := &securityMRs[i]
			newAward := awards.Award{
				SecurityMR: smr,
			}

			for _, award := range awardsList {
				if award.SecurityMR == nil {
					continue
				}
				if smr.WebURL == award.SecurityMR.WebURL {
					newAward.Date = award.Date
					break
				}
			}
			newAwards = append(newAwards, newAward)
		}

		yml, err := yaml.Marshal(newAwards)
		if err != nil {
			return err
		}
		fmt.Fprintf(c.App.Writer, "%s", yml)

		return nil
	}
}

func rewardSecurityMRsCmd(git gitlab.Client) func(*cli.Context) error {
	return func(c *cli.Context) error {

		fmt.Fprintf(c.App.Writer, "Gathering security merge requests: ")
		securityMRs, err := git.SecurityMRs().ListMergeRequests(time.Now().AddDate(0, -monthsBack, 0))
		if err != nil {
			return err
		}

		fmt.Fprintf(c.App.Writer, "%d found ", len(securityMRs))

		// Load the awards to compare with the list
		awardsList, err := awards.LoadAwards(c.String(dataFileFlagName))
		if err != nil {
			return err
		}

		count := len(awardsList)

		for i := range securityMRs {
			smr := &securityMRs[i]
			found := false
			for _, award := range awardsList {
				if award.SecurityMR == nil {
					continue
				}
				if smr.WebURL == award.SecurityMR.WebURL {
					found = true
					break
				}
			}
			if !found {
				awardsList = append(awardsList, awards.Award{
					Date:       now(),
					SecurityMR: smr,
				})
			}
		}

		awardsData, err := yaml.Marshal(awardsList)
		if err != nil {
			return err
		}

		diff := len(awardsList) - count
		fmt.Fprintf(c.App.Writer, "(%d new)\n", diff)

		if diff > 0 {
			fmt.Fprintf(c.App.Writer, "Writing data file (%s): ", c.String(dataFileFlagName))
			err = ioutil.WriteFile(c.String(dataFileFlagName), awardsData, 0600)
			if err != nil {
				return err
			}
			fmt.Fprint(c.App.Writer, "[OK]\n")
		}
		return nil
	}
}
