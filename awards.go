package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"

	"github.com/fatih/color"
	"github.com/google/go-cmp/cmp"
	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
	"sigs.k8s.io/yaml"
)

func validateCmd(c *cli.Context) error {
	color.NoColor = c.Bool("no-color")

	// load file
	b, err := ioutil.ReadFile(filepath.Clean(c.String(dataFileFlagName)))
	if err != nil {
		return err
	}

	aa := awards.Awards{}
	err = yaml.Unmarshal(b, &aa)
	if err != nil {
		return err
	}

	err = aa.Validate()
	if err != nil {
		return err
	}

	// Check if the marshaled version is matching the data file
	m, err := yaml.Marshal(aa)
	if err != nil {
		return err
	}

	if diff := cmp.Diff(b, m); diff != "" {
		return fmt.Errorf("Data file is not consistent with Marshaled awards(-want +got): %s", diff)
	}

	green := color.New(color.FgGreen)
	fmt.Fprintf(c.App.Writer, "[OK] ")
	green.Fprintf(c.App.Writer, "Data file %s is valid\n", c.String(dataFileFlagName))
	return nil
}
