package main

import (
	"encoding/json"
	"fmt"

	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/gitlab"
	"sigs.k8s.io/yaml"
)

func fetchCmd(git gitlab.Client) func(*cli.Context) error {
	return func(c *cli.Context) error {
		nominations, err := git.Nominations().GetNominations()
		if err != nil {
			return err
		}

		var b []byte
		switch c.String("output") {
		case "txt":
			b = nominations.RenderAsText(c.Bool("no-color"))
		case "json":
			b, err = json.Marshal(nominations)
			if err != nil {
				return err
			}
		case "yaml":
			b, err = yaml.Marshal(nominations)
			if err != nil {
				return err
			}
		default:
			return fmt.Errorf("Unknown output format: %s", c.String("output"))
		}

		_, err = fmt.Fprintf(c.App.Writer, "%s", b)
		return err
	}
}
