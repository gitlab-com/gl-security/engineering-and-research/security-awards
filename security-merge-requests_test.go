package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
)

func TestSecurityMR(t *testing.T) {
	t.Run("list", func(t *testing.T) {

		// Create temp file from the awardsYAML struct
		tmpAwardsFile := createTmpAwardsDataFile(t)
		defer os.Remove(tmpAwardsFile.Name()) // clean up

		os.Setenv("GITLAB_API_TOKEN", "asdf123")
		os.Setenv("AWARDS_FILE_PATH", tmpAwardsFile.Name())

		output := new(bytes.Buffer)
		client := &TestClient{}
		app := NewApp(client)
		app.Writer = output

		err := app.Run([]string{os.Args[0], "security-merge-requests", "list"})
		if err != nil {
			t.Fatal(err)
		}

		isLoadTeamCalled(t, client)

		got := output.String()

		want :=
			`- date: new
  security_mr:
    author: igor.drozdov
    category: engineering
    points: 100
    reviewers:
      categories:
        engineering:
        - robotmay_gitlab
        - pks-t
      points: 50
    severity: 1
    web_url: https://gitlab.com/gitlab-org/security/gitaly/-/merge_requests/1565
`

		if diff := cmp.Diff(want, got); diff != "" {
			// full output
			fmt.Printf("got = %+v\n", got)

			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}
	})

	t.Run("reward", func(t *testing.T) {

		// Create temp file from the awardsYAML struct
		tmpAwardsFile := createTmpAwardsDataFile(t)
		defer os.Remove(tmpAwardsFile.Name()) // clean up

		os.Setenv("GITLAB_API_TOKEN", "asdf123")
		os.Setenv("AWARDS_FILE_PATH", tmpAwardsFile.Name())

		output := new(bytes.Buffer)
		client := &TestClient{}
		app := NewApp(client)
		app.Writer = output

		err := app.Run([]string{os.Args[0], "security-merge-requests", "reward"})
		if err != nil {
			t.Fatal(err)
		}

		isLoadTeamCalled(t, client)

		got := output.String()

		want := fmt.Sprintf(`Gathering security merge requests: 1 found (1 new)
Writing data file (%s): [OK]
`, tmpAwardsFile.Name())

		if diff := cmp.Diff(want, got); diff != "" {
			// full output
			fmt.Printf("got = %+v\n", got)

			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}

		t.Run("Saved awards file", func(t *testing.T) {
			savedAwards, err := ioutil.ReadFile(tmpAwardsFile.Name())
			if err != nil {
				t.Fatal(err)
			}

			want :=
				`- date: "2020-11-23"
  nomination:
    author: pks-t
    category: engineering
    council_issue_url: https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1089#note_447389715
    nominated_by: rchan-gitlab
    points: 200
    thumbs_up:
    - dappelt
    - plafoucriere
    web_url: https://gitlab.com/gitlab-org/gitaly/-/merge_requests/2669
- date: "2020-08-23"
  nomination:
    author: igor.drozdov
    category: engineering
    council_issue_url: https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1090#note_447389716
    nominated_by: dappelt
    points: 200
    thumbs_up:
    - rchan-gitlab
    - plafoucriere
    web_url: https://gitlab.com/gitlab-org/gitaly/-/merge_requests/2670
- date: "` + now().Format(awards.LayoutISO) + `"
  security_mr:
    author: igor.drozdov
    category: engineering
    points: 100
    reviewers:
      categories:
        engineering:
        - robotmay_gitlab
        - pks-t
      points: 50
    severity: 1
    web_url: https://gitlab.com/gitlab-org/security/gitaly/-/merge_requests/1565
`

			if diff := cmp.Diff(want, string(savedAwards)); diff != "" {
				t.Errorf("Awards file content mismatch (-want +got):\n%s", diff)
			}
		})

		t.Run("second run, no new", func(t *testing.T) {

			output.Reset()
			err := app.Run([]string{os.Args[0], "security-merge-requests", "reward"})
			if err != nil {
				t.Fatal(err)
			}
			got := output.String()

			want := "Gathering security merge requests: 1 found (0 new)\n"

			if diff := cmp.Diff(want, got); diff != "" {
				// full output
				fmt.Printf("got = %+v\n", got)

				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}
		})

	})
}
