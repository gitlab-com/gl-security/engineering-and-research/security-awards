package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"text/tabwriter"

	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
	"sigs.k8s.io/yaml"
)

func generatePrizesCmd(c *cli.Context) error {
	// load file
	b, err := ioutil.ReadFile(filepath.Clean(c.String("prizes-file")))
	if err != nil {
		return err
	}

	p := awards.Prizes{}
	err = yaml.Unmarshal(b, &p)
	if err != nil {
		return err
	}

	// GitLab Handbook header

	fmt.Fprint(c.App.Writer, `---
layout: handbook-page-toc
title: "Security Awards Prizes"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

`)

	for _, list := range p.PrizesList {
		_, err = fmt.Fprintf(c.App.Writer, "# Prizes %s\n", list.Year)
		periods := [][]awards.CategoryPrize{list.Yearly, list.Q4, list.Q3, list.Q2, list.Q1}
		for i, period := range periods {
			if len(period) > 0 {

				if i == 0 { // first iteration is for the yearly list, then quarters
					fmt.Fprintf(c.App.Writer, "\n## Yearly\n\n")
				} else {
					fmt.Fprintf(c.App.Writer, "\n## %s-Q%d\n\n", list.Year, len(periods)-i)
				}

				w := tabwriter.NewWriter(c.App.Writer, 0, 0, 1, ' ', tabwriter.Debug) // tabwriter.Debug prints `|` on tabs, handy for markdown
				fmt.Fprintln(w, "\t Category \t Rank \t Prize \t")
				fmt.Fprintln(w, "\t -------- \t ---: \t ----- \t")

				rankStart := map[awards.Category]int{}
				for _, prize := range period {
					cat := prize.Category
					rank := fmt.Sprintf("%d-%d", rankStart[cat]+1, prize.RankEnd)
					// If RankEnd is 1, display "1" instead of "1-1"
					if prize.RankEnd == 1 {
						rank = fmt.Sprintf("%d", prize.RankEnd)
					}
					_, err = fmt.Fprintf(w, "\t %s \t %s \t %s \t\n", cat.Name(), rank, prize.Prize.Name)
					rankStart[cat] = prize.RankEnd
				}
				err := w.Flush()
				if err != nil {
					return err
				}
			}
		}
		fmt.Fprintln(c.App.Writer)
	}

	return err
}
