package awards

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/url"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"sigs.k8s.io/yaml"
)

// JSONURL host a *url.URL with special json (un)marshalling
type JSONURL struct {
	*url.URL
}

// JSONDate host a *time.Time with special json (un)marshalling
type JSONDate struct {
	*time.Time
}

// Award holds the data used to track points
type Award struct {
	Date       JSONDate    `json:"date"`
	Nomination *Nomination `json:"nomination,omitempty"`
	SecurityMR *SecurityMR `json:"security_mr,omitempty"`
}

// Awards is a list of Award
type Awards []Award

// GitLabUser is a string with a representation specific to GitLab
type GitLabUser string

// String adds a '@' prefix to the username
func (u GitLabUser) String() string {
	return fmt.Sprintf("@%s", string(u))
}

var gitLabUserFormat = regexp.MustCompile(`^[a-zA-Z0-9\-_\.]+$`)
var errUserFormat = "Invalid GitLab username format: %s"

// Validate checks the format of the GitLabUser string
func (u GitLabUser) Validate() error {
	type user GitLabUser
	gu := user(u)
	if !gitLabUserFormat.MatchString(string(gu)) {
		return fmt.Errorf(errUserFormat, gu)
	}
	return nil
}

// FiscalYear is a year as defined in https://about.gitlab.com/handbook/finance/#fiscal-year
type FiscalYear uint

// String formats a year to a Fiscal Year (
func (y FiscalYear) String() string {
	return fmt.Sprintf("FY%d", y%100)
}

// UnmarshalJSON parses the fiscal year represented as a string (ex: "fy21")
func (y *FiscalYear) UnmarshalJSON(b []byte) error {
	year := strings.Split(strings.ToLower(string(b[1:len(b)-1])), "fy")
	if len(year) < 2 {
		return fmt.Errorf("Fiscal year format error (expected: 'fy\\d{2}'): %s", b)
	}
	i, err := strconv.Atoi(year[1])
	*y = 2000 + FiscalYear(i)
	return err
}

// MarshalJSON marshals the string representation of a fiscal year
func (y FiscalYear) MarshalJSON() ([]byte, error) {
	return json.Marshal(y.String())
}

// UnmarshalJSON parses the url represented as a string
func (j *JSONURL) UnmarshalJSON(b []byte) error {
	url, err := url.Parse(string(b[1 : len(b)-1]))
	j.URL = url
	return err
}

// MarshalJSON marshals the string representation of the underlying URL
func (j JSONURL) MarshalJSON() ([]byte, error) {
	return json.Marshal(j.URL.String())
}

// LayoutISO is the default date format for awards
const LayoutISO = "2006-01-02"

// UnmarshalJSON parses the date formatted with LayoutISO
func (j *JSONDate) UnmarshalJSON(b []byte) error {
	d, err := time.Parse(LayoutISO, string(b[1:len(b)-1]))
	j.Time = &d
	return err
}

// MarshalJSON marshals the date in the LayoutISO format
func (j JSONDate) MarshalJSON() ([]byte, error) {
	if j.Time == nil {
		return json.Marshal("new")
	}
	return json.Marshal(j.Format(LayoutISO))
}

// ToLeaderBoard generates a Leaderboard from the data available in the Awards
func (awards Awards) ToLeaderBoard() Leaderboard {

	// Temporary structs to ease calculations
	type userMap map[GitLabUser]struct {
		Points uint
		Index  uint // used in ties. First appearance in a ranking have precedence
	}
	// Keep category in case user transitioned to another dept/sub-dept
	type categoryMap map[Category]userMap
	type yearlyMap struct {
		Yearly categoryMap
		Q1     categoryMap
		Q2     categoryMap
		Q3     categoryMap
		Q4     categoryMap
	}
	type leaderMap map[FiscalYear]yearlyMap

	l := make(leaderMap)

	// Initialize leadermap from awards
	for _, award := range awards {
		var quarter categoryMap
		y, _ := strconv.ParseUint(award.Date.Format("2006"), 10, 32)
		year := FiscalYear(uint(y))
		month := award.Date.Format("1")

		// Fiscal Years start in February at GitLab
		if month != "1" {
			year++
		}

		// init year
		if _, ok := l[year]; !ok {
			l[year] = yearlyMap{
				Yearly: make(categoryMap),
				Q1:     make(categoryMap),
				Q2:     make(categoryMap),
				Q3:     make(categoryMap),
				Q4:     make(categoryMap),
			}
		}

		switch award.Date.Format("1") {
		case "1":
			quarter = l[year].Q4
		case "2", "3", "4":
			quarter = l[year].Q1
		case "5", "6", "7":
			quarter = l[year].Q2
		case "8", "9", "10":
			quarter = l[year].Q3
		case "11", "12":
			quarter = l[year].Q4
		}

		// Update quarter
		for category, nominees := range award.UsersWithPointsByCategory() {
			c, ok := quarter[category]
			if !ok {
				c = make(userMap)
				quarter[category] = c
			}

			for _, n := range nominees {
				user := c[n.User]
				if user.Points == 0 { // First appearance in this ranking
					user.Index = uint(len(c))
				}
				user.Points += n.Points
				c[n.User] = user

				// Update yearly
				yc, ok := l[year].Yearly[category]
				if !ok {
					yc = make(userMap)
					l[year].Yearly[category] = yc
				}
				user = yc[n.User]
				if user.Points == 0 { // First appearance in this ranking
					user.Index = uint(len(yc))
				}
				user.Points += n.Points
				yc[n.User] = user
			}
		}
	}

	leaderboard := Leaderboard{}
	categories := Categories()

	for year, ym := range l {
		// Fill quarters from the temporary l yearlyMap
		yb := YearlyBoard{Year: year, Quarters: []Quarter{}}
		for _, quarterName := range []string{"Q4", "Q3", "Q2", "Q1"} {
			var dest categoryMap
			switch quarterName {
			case "Q1":
				dest = ym.Q1
			case "Q2":
				dest = ym.Q2
			case "Q3":
				dest = ym.Q3
			case "Q4":
				dest = ym.Q4
			}
			if len(dest) > 0 {
				newQuarter := Quarter{quarterName, make([]Ranking, len(categories))}
				for i, cat := range categories {
					r := Ranking{Category: cat, Nominees: []Nominee{}}
					for user, points := range dest[cat] {
						r.Nominees = append(r.Nominees, Nominee{User: user, Points: points.Points, Index: points.Index})
					}
					sort.Slice(r.Nominees, func(i, j int) bool { return r.Nominees[i].Index < r.Nominees[j].Index })
					sort.Stable(sort.Reverse(ByPoints(r.Nominees)))
					newQuarter.Ranking[i] = r
				}
				yb.Quarters = append(yb.Quarters, newQuarter)
			}
		}

		// Yearly ranking
		// Skip for FY21, as we started at Q4 only
		if year > 2021 {
			newYearRanking := make([]Ranking, len(categories))
			for i, cat := range categories {
				r := Ranking{Category: cat, Nominees: []Nominee{}}
				for user, points := range ym.Yearly[cat] {
					r.Nominees = append(r.Nominees, Nominee{User: user, Points: points.Points, Index: points.Index})
				}
				sort.Slice(r.Nominees, func(i, j int) bool { return r.Nominees[i].Index < r.Nominees[j].Index })
				sort.Stable(sort.Reverse(ByPoints(r.Nominees)))
				newYearRanking[i] = r
			}
			yb.Ranking = newYearRanking
		}

		leaderboard = append(leaderboard, yb)
	}
	// Sort leaderboard by years
	sort.Slice(leaderboard, func(i, j int) bool { return leaderboard[i].Year > leaderboard[j].Year })

	return leaderboard
}

var errDateInFuture = "Awards date can't be in the future: %v"
var errUnknownCategory = "Unknown category: %s"
var errDuplicateURL = "Duplicate URL in Awards: %s"

// Validate validates the current award
func (award Award) Validate() error {
	// Award Date can't be in the future
	if award.Date.After(time.Now()) {
		return fmt.Errorf(errDateInFuture, award.Date)
	}

	if award.Nomination != nil {
		if err := award.Nomination.Validate(); err != nil {
			return err
		}
	}

	if award.SecurityMR != nil {
		if err := award.SecurityMR.Validate(); err != nil {
			return err
		}
	}

	if award.Nomination == nil && award.SecurityMR == nil {
		return errors.New("Awards must have either a Nomination of a SecurityMR")
	}

	return nil
}

// Validate validates the awards struct data
func (awards Awards) Validate() error {
	// urlMap is used to store urls declared in the data file, so we can detect duplicates
	urlMap := make(map[GitLabURL]struct{})

	for _, award := range awards {
		if err := award.Validate(); err != nil {
			return err
		}

		// Awards can't have duplicate URLs
		if award.Nomination != nil {
			if _, ok := urlMap[award.Nomination.WebURL]; ok {
				return fmt.Errorf(errDuplicateURL, award.Nomination.WebURL)
			}
			urlMap[award.Nomination.WebURL] = struct{}{}
		}
	}
	return nil
}

// LoadAwards loads Awards from a file path
func LoadAwards(file string) (Awards, error) {
	var awards Awards

	b, err := ioutil.ReadFile(filepath.Clean(file))
	if err != nil {
		return awards, err
	}

	err = yaml.Unmarshal(b, &awards)
	return awards, err
}

// UsersWithPointsByCategory returns all users from an awards, with their respective points
func (award Award) UsersWithPointsByCategory() map[Category][]Nominee {
	ret := make(map[Category][]Nominee)
	if award.Nomination != nil {
		ret = award.Nomination.UsersWithPointsByCategory()
	}

	if award.SecurityMR != nil {
		for category, nominees := range award.SecurityMR.UsersWithPointsByCategory() {
			for _, n := range nominees {
				ret[category] = append(ret[category], Nominee{User: n.User, Points: n.Points})
			}
		}
	}
	return ret
}
