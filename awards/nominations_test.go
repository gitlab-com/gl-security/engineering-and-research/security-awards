package awards

import (
	"fmt"
	"testing"
)

func TestNominations(t *testing.T) {
	t.Run("Validate", func(t *testing.T) {
		nomination := Nomination{
			Category:        CategoryEngineering,
			WebURL:          "https://gitlab.com/gitlab-org/gitaly/-/merge_requests/2669",
			CouncilIssueURL: "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1089#note_447389715",
			Author:          GitLabUser("user1"),
			ThumbsUP:        []GitLabUser{"dappelt", "plafoucriere"},
			Points:          200,
			NominatedBy:     "rchan-gitlab",
		}
		t.Run("Invalid user", func(t *testing.T) {
			n := nomination
			n.Author = "@invalidOne"
			if err := n.Validate(); err.Error() != "Nomination.Author error: Invalid GitLab username format: @invalidOne" {
				t.Errorf("Expected error: %s, got error: %s", fmt.Errorf(errUserFormat, n.Author), err)
			}
		})
	})
}
