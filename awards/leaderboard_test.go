package awards

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLeaderboard(t *testing.T) {
	nomination := Nomination{
		Category:        CategoryEngineering,
		WebURL:          "https://gitlab.com/gitlab-org/gitaly/-/merge_requests/2669",
		CouncilIssueURL: "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1089#note_447389715",
		Author:          GitLabUser("user1"),
		ThumbsUP:        []GitLabUser{"dappelt", "plafoucriere"},
		Points:          200,
		NominatedBy:     "rchan-gitlab",
	}
	nomination2 := nomination
	nomination2.Author = "user2"

	nomination2With440pts := nomination2
	nomination2With440pts.Points = 400

	awardsStruct := Awards{
		{
			Date:       toJSONDate("2020-11-23", t),
			Nomination: &nomination,
		},
		{
			Date:       toJSONDate("2020-08-23", t),
			Nomination: &nomination,
		},
		{
			Date:       toJSONDate("2020-08-23", t),
			Nomination: &nomination,
		},
		{
			Date:       toJSONDate("2020-01-23", t),
			Nomination: &nomination2,
		},
		{
			Date:       toJSONDate("2020-08-23", t),
			Nomination: &nomination2,
		},
		{
			Date:       toJSONDate("2020-11-23", t),
			Nomination: &nomination,
		},
		{
			Date:       toJSONDate("2020-11-23", t),
			Nomination: &nomination2With440pts,
		},
		{
			Date: toJSONDate("2020-11-25", t),
			SecurityMR: &SecurityMR{
				Category: CategoryEngineering,
				WebURL:   "https://gitlab.com/gitlab-org/security/gitaly/-/merge_requests/1565",
				Author:   GitLabUser("user3"),
				Points:   100,
				Severity: Severity1,
				Reviewers: Reviewers{
					Points: 50,
					Categories: map[Category][]GitLabUser{
						CategoryEngineering: []GitLabUser{"user4", "user5"},
					},
				},
			},
		},
		{ // SecurityMR with no reviewers
			Date: toJSONDate("2020-11-26", t),
			SecurityMR: &SecurityMR{
				Category: CategoryEngineering,
				WebURL:   "https://gitlab.com/gitlab-org/security/gitaly/-/merge_requests/1566",
				Author:   GitLabUser("user3"),
				Points:   100,
				Severity: Severity2,
				Reviewers: Reviewers{
					Points:     50,
					Categories: map[Category][]GitLabUser{},
				},
			},
		},
	}

	leaderboard := Leaderboard{
		YearlyBoard{
			Year: 2021,
			Quarters: []Quarter{
				{Quarter: "Q4",
					Ranking: []Ranking{
						{
							Category: "dev",
							Nominees: []Nominee{},
						},
						{
							Category: "engineering",
							Nominees: []Nominee{
								{User: "user1", Points: 400, Index: 0},
								{User: "user2", Points: 400, Index: 1},
								{User: "user3", Points: 200, Index: 2},
								{User: "user4", Points: 50, Index: 3},
								{User: "user5", Points: 50, Index: 4},
							},
						},
						{
							Category: "non-engineering",
							Nominees: []Nominee{},
						},
						{
							Category: "community",
							Nominees: []Nominee{},
						},
					},
				},
				{
					Quarter: "Q3",
					Ranking: []Ranking{
						{
							Category: "dev",
							Nominees: []Nominee{},
						},
						{
							Category: "engineering",
							Nominees: []Nominee{
								{User: "user1", Points: 400, Index: 0},
								{User: "user2", Points: 200, Index: 1},
							},
						},
						{
							Category: "non-engineering",
							Nominees: []Nominee{},
						},
						{
							Category: "community",
							Nominees: []Nominee{},
						},
					},
				},
			},
		},
		YearlyBoard{
			Year:    2020,
			Ranking: nil,
			Quarters: []Quarter{
				{Quarter: "Q4",
					Ranking: []Ranking{
						{
							Category: "dev",
							Nominees: []Nominee{},
						},
						{
							Category: "engineering",
							Nominees: []Nominee{
								{User: "user2", Points: 200},
							},
						},
						{
							Category: "non-engineering",
							Nominees: []Nominee{},
						},
						{
							Category: "community",
							Nominees: []Nominee{},
						},
					},
				},
			},
		},
	}

	t.Run("Generate from awards", func(t *testing.T) {
		want := leaderboard
		got := awardsStruct.ToLeaderBoard()

		if diff := cmp.Diff(want, got); diff != "" {
			t.Errorf("ToLeaderBoard() mismatch (-want +got):\n%s", diff)
		}

	})

	t.Run("Generate Markdown output", func(t *testing.T) {
		want := `---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY21

## Yearly

No yearly board for FY21.

## FY21-Q4

### Development

Category is empty

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@user1](https://gitlab.com/user1) | 1 | 400 |
| [@user2](https://gitlab.com/user2) | 2 | 400 |
| [@user3](https://gitlab.com/user3) | 3 | 200 |
| [@user4](https://gitlab.com/user4) | 4 | 50 |
| [@user5](https://gitlab.com/user5) | 5 | 50 |

### Non-Engineering

Category is empty

### Community

Category is empty

## FY21-Q3

### Development

Category is empty

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@user1](https://gitlab.com/user1) | 1 | 400 |
| [@user2](https://gitlab.com/user2) | 2 | 200 |

### Non-Engineering

Category is empty

### Community

Category is empty

# Leaderboard FY20

## Yearly

No yearly board for FY20.

## FY20-Q4

### Development

Category is empty

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@user2](https://gitlab.com/user2) | 1 | 200 |

### Non-Engineering

Category is empty

### Community

Category is empty

`
		got := leaderboard.ToMarkdown()

		if diff := cmp.Diff(want, got); diff != "" {
			t.Errorf("ToLeaderBoard() mismatch (-want +got):\n%s", diff)
		}

	})
}
