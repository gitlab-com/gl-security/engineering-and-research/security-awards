package awards

import (
	"fmt"
	"net/url"
	"strings"
)

// Severity reflects the "~severity::x" labels
type Severity int

const (
	// Severity1 is label "~severity::1"
	Severity1 Severity = iota + 1
	// Severity2 is label "~severity::2"
	Severity2
	// Severity3 is label "~severity::3"
	Severity3
	// Severity4 is label "~severity::4"
	Severity4
)

// SecurityMR represents a Merge Request with the ~security label and one of the ~sevirity::x one.
// These Merge Requests are created and merged in a security-dedicated repo, as per our Security Release workflow:
// https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/developer.md
type SecurityMR struct {
	WebURL    GitLabURL  `json:"web_url"`
	Severity  Severity   `json:"severity"`
	Author    GitLabUser `json:"author"`
	Category  Category   `json:"category"` // Category of the Author
	Points    uint       `json:"points"`   // Amount of points for the Author
	Reviewers Reviewers  `json:"reviewers"`
}

// Reviewers are Merge Request reviewers and are rewarded automatically for Security MRs.
type Reviewers struct {
	Categories map[Category][]GitLabUser `json:"categories"`
	Points     uint                      `json:"points"`
}

// Validate validates SecurityMR data
func (s SecurityMR) Validate() error {
	secRepoURL := "https://gitlab.com/gitlab-org/security"
	if !strings.HasPrefix(string(s.WebURL), secRepoURL) {
		return fmt.Errorf("SecurityMR.WebURL must start with %q", secRepoURL)
	}

	if _, err := url.Parse(string(s.WebURL)); err != nil {
		return fmt.Errorf("SecurityMR.WebURL parsing error: %s", err)
	}

	if err := s.Author.Validate(); err != nil {
		return fmt.Errorf("SecurityMR.Author error: %s", err)
	}

	if err := s.Category.Validate(); err != nil {
		return err
	}

	for category, users := range s.Reviewers.Categories {
		if err := category.Validate(); err != nil {
			return fmt.Errorf("SecurityMR.Reviewers category error: %s", err)
		}
		// each user of each category
		for _, u := range users {
			if err := u.Validate(); err != nil {
				return fmt.Errorf("SecurityMR.Reviewers user error: %s", err)
			}
		}
	}

	if s.Severity < 1 || s.Severity > 4 {
		return fmt.Errorf("SecurityMR.Severity not in 1-4: %d", s.Severity)
	}

	if s.Points < 1 {
		return fmt.Errorf("SecurityMR.Points must be >= 1: %d", s.Points)
	}

	// If there are reviewers, they must have points
	if len(s.Reviewers.Categories) > 0 && s.Reviewers.Points < 1 {
		return fmt.Errorf("SecurityMR.Reviewers.Points must be >= 1: %d", s.Reviewers.Points)
	}

	// Reviewers.Category can't be empty
	for c, u := range s.Reviewers.Categories {
		if len(u) == 0 {
			return fmt.Errorf("SecurityMR.Reviewers.Categories: %s is empty", c)
		}
	}

	return nil
}

// UsersWithPointsByCategory returns all the users awarded (with their points), grouped by Category
func (s SecurityMR) UsersWithPointsByCategory() map[Category][]Nominee {
	ret := map[Category][]Nominee{
		s.Category: []Nominee{
			{User: s.Author, Points: s.Points},
		},
	}

	for category, users := range s.Reviewers.Categories {
		for _, user := range users {
			ret[category] = append(ret[category], Nominee{User: user, Points: s.Reviewers.Points})
		}
	}

	return ret
}
