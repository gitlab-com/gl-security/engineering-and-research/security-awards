package awards

import (
	"fmt"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"sigs.k8s.io/yaml"
)

func toJSONDate(dateStr string, t *testing.T) JSONDate {
	jd := JSONDate{}
	d, err := time.Parse("2006-01-02", dateStr)
	if err != nil {
		t.Fatal(err)
	}
	jd.Time = &d
	return jd
}

var awardsYAML string = `- date: "2020-11-23"
  nomination:
    author: pks-t
    category: engineering
    council_issue_url: https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1089#note_447389715
    nominated_by: rchan-gitlab
    points: 200
    thumbs_up:
    - dappelt
    - plafoucriere
    web_url: https://gitlab.com/gitlab-org/gitaly/-/merge_requests/2669
- date: "2020-08-23"
  nomination:
    author: igor.drozdov
    category: engineering
    council_issue_url: https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1090#note_447389716
    nominated_by: rchan-gitlab
    points: 200
    thumbs_up:
    - dappelt
    - plafoucriere
    web_url: https://gitlab.com/gitlab-org/gitaly/-/merge_requests/2670
- date: "2021-03-17"
  security_mr:
    author: igor.drozdov
    category: engineering
    points: 100
    reviewers:
      categories:
        engineering:
        - robotmay_gitlab
        - pks-t
      points: 50
    severity: 1
    web_url: https://gitlab.com/gitlab-org/security/gitaly/-/merge_requests/1565
`

func TestAwards(t *testing.T) {

	awardsStruct := Awards{
		{
			Date: toJSONDate("2020-11-23", t),
			Nomination: &Nomination{
				Category:        CategoryEngineering,
				WebURL:          "https://gitlab.com/gitlab-org/gitaly/-/merge_requests/2669",
				CouncilIssueURL: "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1089#note_447389715",
				Author:          GitLabUser("pks-t"),
				ThumbsUP:        []GitLabUser{"dappelt", "plafoucriere"},
				Points:          200,
				NominatedBy:     "rchan-gitlab",
			},
		},
		{
			Date: toJSONDate("2020-08-23", t),
			Nomination: &Nomination{
				Category:        CategoryEngineering,
				WebURL:          "https://gitlab.com/gitlab-org/gitaly/-/merge_requests/2670",
				CouncilIssueURL: "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1090#note_447389716",
				Author:          GitLabUser("igor.drozdov"),
				ThumbsUP:        []GitLabUser{"dappelt", "plafoucriere"},
				Points:          200,
				NominatedBy:     "rchan-gitlab",
			},
		},
		{
			Date: toJSONDate("2021-03-17", t),
			SecurityMR: &SecurityMR{
				Category: CategoryEngineering,
				WebURL:   "https://gitlab.com/gitlab-org/security/gitaly/-/merge_requests/1565",
				Author:   GitLabUser("igor.drozdov"),
				Points:   100,
				Severity: Severity1,
				Reviewers: Reviewers{
					Points: 50,
					Categories: map[Category][]GitLabUser{
						CategoryEngineering: []GitLabUser{"robotmay_gitlab", "pks-t"},
					},
				},
			},
		},
	}

	t.Run("YAML Unmarshal", func(t *testing.T) {
		var got Awards
		want := awardsStruct
		if err := yaml.Unmarshal([]byte(awardsYAML), &got); err != nil {
			t.Error("Unable to Unmarshal awards:", err)
		}

		if diff := cmp.Diff(want, got); diff != "" {
			t.Errorf("Unmarshal() mismatch (-want +got):\n%s", diff)
		}
	})

	t.Run("YAML Marshal", func(t *testing.T) {
		want := awardsYAML
		got, err := yaml.Marshal(awardsStruct)
		if err != nil {
			t.Fatal("Unable to Marshal awards:", err)
		}

		if diff := cmp.Diff(want, string(got)); diff != "" {
			t.Errorf("Marshal() mismatch (-want +got):\n%s", diff)
		}
	})

	t.Run("Validate", func(t *testing.T) {

		t.Run("Valid", func(t *testing.T) {
			if err := awardsStruct.Validate(); err != nil {
				t.Error("Should be valid, got error:", err)
			}
		})

		t.Run("Invalid, date in the future", func(t *testing.T) {
			future := time.Now().Add(24 * time.Hour) // store original value
			award := awardsStruct[0]
			award.Date = JSONDate{&future}
			if err := award.Validate(); err.Error() != fmt.Errorf(errDateInFuture, award.Date).Error() {
				t.Errorf("Expected error: %s, got error: %s", fmt.Errorf(errDateInFuture, award.Date), err)
			}
		})

		t.Run("Invalid, duplicate URL", func(t *testing.T) {
			orgURL := awardsStruct[0] // store original value
			awardsStruct[0].Nomination.WebURL = awardsStruct[1].Nomination.WebURL

			if err := awardsStruct.Validate(); err.Error() != fmt.Errorf(errDuplicateURL, awardsStruct[0].Nomination.WebURL).Error() {
				t.Errorf("Expected error: %v, got error: %s", errDuplicateURL, err)
			}
			awardsStruct[0] = orgURL // restore original value
		})

		t.Run("Empty award", func(t *testing.T) {
			award := Award{
				Date: toJSONDate("2020-11-23", t),
			}
			want := "Awards must have either a Nomination of a SecurityMR"
			if err := award.Validate(); err.Error() != want {
				t.Errorf("Expected error: %s, got error: %s", want, err)
			}
		})
	})
}

func TestFiscalYear(t *testing.T) {
	t.Run("YAML Marshal", func(t *testing.T) {
		want := "FY21\n"
		got, err := yaml.Marshal(FiscalYear(2021))
		if err != nil {
			t.Fatal("Unable to Marshal FiscalYear:", err)
		}

		if diff := cmp.Diff(want, string(got)); diff != "" {
			t.Errorf("Marshal() mismatch (-want +got):\n%s", diff)
		}
	})
	t.Run("YAML Unmarshal", func(t *testing.T) {
		want := FiscalYear(2021)
		var got FiscalYear
		err := yaml.Unmarshal([]byte("fy21"), &got)
		if err != nil {
			t.Fatal("Unable to Unmarshal FiscalYear:", err)
		}

		if diff := cmp.Diff(want, got); diff != "" {
			t.Errorf("Marshal() mismatch (-want +got):\n%s", diff)
		}
	})
	t.Run("YAML Unmarshal invalid format", func(t *testing.T) {
		var got FiscalYear
		err := yaml.Unmarshal([]byte("21"), &got)
		if err.Error() != "error unmarshaling JSON: while decoding JSON: Fiscal year format error (expected: 'fy\\d{2}'): 21" {
			t.Fatal("Unmarshal FiscalYear should raise format error, got: ", err)
		}
	})
}
