package awards

import (
	"io/ioutil"
	"path/filepath"

	"sigs.k8s.io/yaml"
)

// IsMemberOfSecurityDept returns true if the user if part of the Security Department
func (t GitLabTeamMember) IsMemberOfSecurityDept() bool {
	for _, d := range t.Departments {
		if d == "Security Department" {
			return true
		}
	}
	return false
}

// GitLabTeamMember is a GitLab Team Member or a Core Team Member
type GitLabTeamMember struct {
	UserName    GitLabUser `json:"gitlab"`
	Departments []string
}

// GitLabTeam is the whole list of GitLab Team Members and Core Team Members
type GitLabTeam []GitLabTeamMember

// LoadTeam loads a GitLabTeam from a file path
func LoadTeam(file string) (GitLabTeam, error) {
	var team GitLabTeam

	b, err := ioutil.ReadFile(filepath.Clean(file))
	if err != nil {
		return team, err
	}

	err = yaml.Unmarshal(b, &team)
	return team, err
}

// Find returns a *GitLabTeamMember if the userName is part of the team, nil otherwise
func (t GitLabTeam) Find(user GitLabUser) *GitLabTeamMember {
	for _, member := range t {
		if member.UserName == user {
			return &member
		}
	}
	return nil
}

// UserCategory returns the Category of a Team member, or CategoryCommunity if the user is not part of the Team
func (t GitLabTeam) UserCategory(user GitLabUser) Category {
	var c Category
	// If the author is not part of the team.yml file, it's a community contribution
	c = CategoryCommunity

	for _, member := range t {
		if member.UserName == user {
			c = CategoryFromDepartments(member.Departments)
			break
		}
	}
	return c
}
