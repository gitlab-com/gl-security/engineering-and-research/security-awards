package awards

// Prize is describing a single prize to give away
type Prize struct {
	Name     string  `json:"name"`
	Value    float64 `json:"value"`
	Currency string  `json:"currency"`
}

// CategoryPrize defines the prizes to attribute to winners, based on the RankEnd field
// Declaration of CategoryPrize must be sequential in the yaml file, so a
// category with 2 different prizes should be 2 consecutive lines in the file
type CategoryPrize struct {
	Category Category `json:"category"`
	RankEnd  int      `json:"rank_end"`
	Prize    Prize    `json:"prize"`
}

// PrizesList organizes CategoryPrizes by period
type PrizesList []struct {
	Year   FiscalYear      `json:"year"`
	Yearly []CategoryPrize `json:"yearly"`
	Q1     []CategoryPrize `json:"q1"`
	Q2     []CategoryPrize `json:"q2"`
	Q3     []CategoryPrize `json:"q3"`
	Q4     []CategoryPrize `json:"q4"`
}

// Prizes is the top wrapper of PrizesList
type Prizes struct {
	PrizesList `json:"prizes"`
}
