package awards

import "fmt"

// Category is the category of the user awarded, among "dev", "engineering" , "non-engineering", "community"
type Category string

const (
	// CategoryDev is the Development category
	CategoryDev = "dev"
	// CategoryEngineering is the Engineering category
	CategoryEngineering = "engineering"
	// CategoryNonEngineering is the Non-Engineering category (rest of the company)
	CategoryNonEngineering = "non-engineering"
	// CategoryCommunity is the Community category
	CategoryCommunity = "community"
	// CategoryAll is all categories
	CategoryAll = ""
)

// Categories returns the list of all Categories
func Categories() []Category {
	return []Category{CategoryDev, CategoryEngineering, CategoryNonEngineering, CategoryCommunity}
}

// CategoryFromDepartments return the Category corresponding to a list of departments
func CategoryFromDepartments(departments []string) Category {
	if len(departments) == 1 && departments[0] == "Core Team" {
		return CategoryCommunity
	}

	cat := Category(CategoryNonEngineering)
	for _, d := range departments {
		switch d {
		case "Development Department":
			return CategoryDev
		case "Engineering Function":
			cat = CategoryEngineering // Store and continue
		}
	}
	return cat
}

// Name returns the long name of a Category
func (c Category) Name() string {
	switch c {
	case CategoryDev:
		return "Development"
	case CategoryEngineering:
		return "Engineering"
	case CategoryNonEngineering:
		return "Non-Engineering"
	case CategoryCommunity:
		return "Community"
	case CategoryAll:
		return "All"
	}
	return "Unknown category"
}

// Validate validates a Category
func (c Category) Validate() error {
	// Category must be one of the define categories
	switch c {
	case CategoryDev, CategoryEngineering, CategoryNonEngineering, CategoryCommunity:
	default:
		return fmt.Errorf(errUnknownCategory, c)
	}

	return nil
}
