package awards

import (
	"bytes"
	"fmt"
	"net/url"
	"sort"

	"github.com/fatih/color"
)

// Nomination is an issue or a Merge Request nominated (with the ~security-awards::nomination label
type Nomination struct {
	Category        Category     `json:"category"`
	WebURL          GitLabURL    `json:"web_url"`
	Title           string       `json:"-"`
	Author          GitLabUser   `json:"author"`
	NominatedBy     GitLabUser   `json:"nominated_by"`
	CouncilIssueURL string       `json:"council_issue_url"`
	ThumbsUP        []GitLabUser `json:"thumbs_up"`
	Points          uint         `json:"points"`
}

// Nominations are an array of Nomination
type Nominations []Nomination

// CouncilIssue is an issue in the gitlab-com/gl-security/security-department-meta used to vote for nominations
type CouncilIssue struct {
	WebURL       GitLabURL `json:"web_url"`
	Title        string    `json:"title,omitempty"`
	IID          int       `json:"iid,omitempty"`
	Confidential bool      `json:"confidential,omitempty"`
}

// IsNew returns true if the nomination doesn't belong to any council issue
func (n Nomination) IsNew() bool {
	return n.CouncilIssueURL == ""
}

// GroupByCouncilIssues groups the nominations by their Council Issue.
// The ones which don't belong to a Council Issue already are in the "New" key
func (nn Nominations) GroupByCouncilIssues() map[string]Nominations {
	m := make(map[string]Nominations)

	for _, nomination := range nn {
		if nomination.IsNew() {
			m["New"] = append(m["New"], nomination)
		} else {
			u, _ := url.Parse(nomination.CouncilIssueURL)
			u.Fragment = ""
			m[u.String()] = append(m[u.String()], nomination)
		}
	}
	return m
}

// RenderAsText renders the nominations as text
func (nn Nominations) RenderAsText(noColor bool) []byte {
	color.NoColor = noColor

	var b bytes.Buffer
	renderMap := nn.GroupByCouncilIssues()

	// Make the output order stable
	councilIssueTitles := []string{}
	for k := range renderMap {
		councilIssueTitles = append(councilIssueTitles, k)
	}
	sort.Slice(councilIssueTitles, func(i, j int) bool {
		if councilIssueTitles[i] == "New" {
			return true
		}
		return councilIssueTitles[i] < councilIssueTitles[j]
	})

	// Render all council issues
	for _, title := range councilIssueTitles {
		header := color.New(color.FgCyan, color.Bold)
		header.Fprintf(&b, "[%s]\n\n", title)

		bold := color.New(color.Bold).SprintFunc()

		// render all their nominations
		for _, nomination := range renderMap[title] {
			fmt.Fprintf(&b, "  * %s - %s [%s]\n", bold(nomination.Title), nomination.WebURL, nomination.NominatedBy)
		}

		// end with a line break
		fmt.Fprintln(&b)
	}
	return b.Bytes()
}

// Validate validates a Nomination
func (n Nomination) Validate() error {
	// Validate author format, especially to prevent duplicate '@' start char.
	if err := n.Author.Validate(); err != nil {
		return fmt.Errorf("Nomination.Author error: %s", err.Error())
	}

	// Validate nominated_by format, especially to prevent duplicate '@' start char.
	// TODO: Validate the user is also part of AppSec
	if err := n.NominatedBy.Validate(); err != nil {
		return fmt.Errorf("Nomination.NominatedBy error: %s", err.Error())
	}

	if err := n.Category.Validate(); err != nil {
		return err
	}

	for _, u := range []string{string(n.WebURL), string(n.CouncilIssueURL)} {
		if _, err := url.Parse(u); err != nil {
			return err
		}
	}

	if n.Points < 1 {
		return fmt.Errorf("Invalid Points (must be >= 1): %d", n.Points)
	}

	return nil
}

// UsersWithPointsByCategory returns all the users awarded (with their points), grouped by Category
func (n Nomination) UsersWithPointsByCategory() map[Category][]Nominee {
	return map[Category][]Nominee{
		n.Category: []Nominee{
			{User: n.Author, Points: n.Points},
		},
	}
}
