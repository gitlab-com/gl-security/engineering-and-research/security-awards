package awards

import "testing"

func TestCategoryFromDepartments(t *testing.T) {
	tests := []struct {
		departments []string
		want        Category
	}{
		{[]string{"Engineering Function", "Development Department", "Dev Section", "Ecosystem BE Team", "Merge Request Buddy", "Merge Request coach"}, CategoryDev},
		{[]string{"Development Department", "Engineering Function", "Dev Section", "Ecosystem BE Team", "Merge Request Buddy", "Merge Request coach"}, CategoryDev}, // order of "Development Department" doesn't matter
		{[]string{"Engineering Function", "Quality Department", "Engineering Productivity Team", "Merge Request coach", "Core Team"}, CategoryEngineering},
		{[]string{"Core Team", "Engineering Function", "Development Department", "Growth Section", "Product Intelligence Team"}, CategoryDev},
		{[]string{"Engineering Function", "UX Department", "Secure UX Team"}, CategoryEngineering},
		{[]string{"Customer Success"}, CategoryNonEngineering},
		{[]string{"Customer Success", "Core Team"}, CategoryNonEngineering}, // Core Team along with any other department is an employee
		{[]string{"Core Team"}, CategoryCommunity}, // Core Team alone is a community contributor
	}

	for _, tc := range tests {
		if c := CategoryFromDepartments(tc.departments); c != tc.want {
			t.Errorf("CategoryFromDepartments(%#v): Expected %q, got %q", tc.departments, tc.want, c)
		}
	}
}
