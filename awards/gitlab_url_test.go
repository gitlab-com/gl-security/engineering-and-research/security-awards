package awards

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestGitLabURL(t *testing.T) {
	testCases := []struct {
		webURL      GitLabURL
		ProjectPath string
		Type        string
		ID          int
	}{
		{"https://gitlab.com/gitlab-org/gitaly/-/issues/29420", "gitlab-org/gitaly", "issues", 29420},
		{"https://gitlab.com/gitlab-org/gitlab/-/merge_requests/16487", "gitlab-org/gitlab", "merge_requests", 16487},
		{"https://gitlab.com/gitlab-com/subproject/subsubproject/-/merge_requests/1", "gitlab-com/subproject/subsubproject", "merge_requests", 1},
	}
	for _, tc := range testCases {
		u := tc.webURL
		if diff := cmp.Diff(tc.ProjectPath, u.ProjectPath()); diff != "" {
			t.Errorf("ProjectPath output mismatch (-want +got): %s", diff)
		}
		if diff := cmp.Diff(tc.Type, u.Type()); diff != "" {
			t.Errorf("Type output mismatch (-want +got): %s", diff)
		}
		if diff := cmp.Diff(tc.ID, u.ID()); diff != "" {
			t.Errorf("ID output mismatch (-want +got): %s", diff)
		}
	}
}
