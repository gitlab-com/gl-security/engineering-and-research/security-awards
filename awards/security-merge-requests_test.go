package awards

import "testing"

func TestSecurityMR(t *testing.T) {
	s := SecurityMR{
		Category: CategoryEngineering,
		WebURL:   "https://gitlab.com/gitlab-org/security/gitaly/-/merge_requests/1565",
		Author:   GitLabUser("igor.drozdov"),
		Points:   100,
		Severity: Severity1,
		Reviewers: Reviewers{
			Points: 50,
			Categories: map[Category][]GitLabUser{
				CategoryEngineering: {"robotmay_gitlab", "pks-t"},
			},
		},
	}

	t.Run("Validate", func(t *testing.T) {
		t.Run("Valid", func(t *testing.T) {
			if err := s.Validate(); err != nil {
				t.Error(err)
			}
		})

		t.Run("No reviewers is valid", func(t *testing.T) {
			sav := s.Reviewers.Categories
			s.Reviewers.Categories = map[Category][]GitLabUser{}
			if err := s.Validate(); err != nil {
				t.Error(err)
			}
			s.Reviewers.Categories = sav // restore
		})

		t.Run("Invalid WebURL prefix", func(t *testing.T) {
			s2 := s
			s2.WebURL = "https://gitlab.com/gitlab-org/gitaly/-/merge_requests/1565"

			if err := s2.Validate(); err.Error() != "SecurityMR.WebURL must start with \"https://gitlab.com/gitlab-org/security\"" {
				t.Errorf("Expected WebURL to be invalid, got: %s", err)
			}
		})

		t.Run("Invalid WebURL format", func(t *testing.T) {
			s2 := s
			s2.WebURL = "https://"

			if err := s2.Validate(); err.Error() != "SecurityMR.WebURL must start with \"https://gitlab.com/gitlab-org/security\"" {
				t.Errorf("Expected WebURL to be invalid, got: %s", err)
			}
		})

		t.Run("Invalid Author empty", func(t *testing.T) {
			s2 := s
			s2.Author = ""

			if err := s2.Validate(); err.Error() != "SecurityMR.Author error: Invalid GitLab username format: " {
				t.Errorf("Expected Author to be invalid, got: %s", err)
			}
		})

		t.Run("Invalid Reviewers user empty", func(t *testing.T) {
			sav := s.Reviewers.Categories[CategoryEngineering][0] //save
			s.Reviewers.Categories[CategoryEngineering][0] = ""

			if err := s.Validate(); err.Error() != "SecurityMR.Reviewers user error: Invalid GitLab username format: " {
				t.Errorf("Expected Reviewers user to be invalid, got: %s", err)
			}
			s.Reviewers.Categories[CategoryEngineering][0] = sav // restore
		})

		t.Run("Invalid Points", func(t *testing.T) {
			s2 := s
			s2.Points = 0

			if err := s2.Validate(); err.Error() != "SecurityMR.Points must be >= 1: 0" {
				t.Errorf("Expected points to be invalid, got: %s", err)
			}
		})
		t.Run("Invalid Reviewers points", func(t *testing.T) {
			s2 := s
			s2.Reviewers.Points = 0

			if err := s2.Validate(); err.Error() != "SecurityMR.Reviewers.Points must be >= 1: 0" {
				t.Errorf("Expected Reviewers points to be invalid, got: %s", err)
			}
		})
	})

}
