package awards

import (
	"bytes"
	"fmt"
	"text/template"
)

// Ranking holds the rankings per categories
type Ranking struct {
	Category Category
	Nominees []Nominee
}

// Quarter hold data for a quarter ranking
type Quarter struct {
	Quarter string
	Ranking []Ranking
}

// YearlyBoard holds data for a whole year (yearly and 4 quarterly rankings)
type YearlyBoard struct {
	Year     FiscalYear
	Ranking  []Ranking
	Quarters []Quarter
}

// Leaderboard is a global ranking, for all the years covered by the Awards
// While Awards are raw data about participants and points, a Leaderboard aggregate data.
// Positions in arrays are used to reflect the rank of participants.
type Leaderboard []YearlyBoard

const boardTmpl = `---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

{{- define "category_ranking" }}
{{ range . -}}
### {{ .Category.Name }}
{{ if eq (len .Nominees) 0 }}
Category is empty
{{ else }}
| Nominee | Rank | Points |
| ------- | ----:| ------:|
{{- range $rank, $nominee := .Nominees }}
| {{linkToUser $nominee.User}} | {{ inc $rank }} | {{ $nominee.Points }} |
{{- end }}
{{ end }}
{{ end -}}
{{ end }}

{{ range $year := . -}}
# Leaderboard {{.Year}}

## Yearly
{{- if eq (len .Ranking) 0  }}

No yearly board for {{.Year}}.

{{ else }}
{{ template "category_ranking" .Ranking }}
{{- end }}
{{- range .Quarters -}}
## {{ $year.Year}}-{{.Quarter}}
{{ template "category_ranking" .Ranking }}
{{- end -}}
{{- end -}}
`

// ToMarkdown exports a Leaderboard to its markdown representation
func (l Leaderboard) ToMarkdown() string {
	t := template.Must(template.New("leaderboard").Funcs(template.FuncMap{
		"inc": func(i int) int {
			return i + 1
		},
		"linkToUser": func(u GitLabUser) string {
			type stripped GitLabUser
			return fmt.Sprintf("[%s](https://gitlab.com/%s)", u, stripped(u))
		},
	}).Parse(boardTmpl))

	var output bytes.Buffer
	err := t.Execute(&output, l)
	if err != nil {
		panic(err)
	}

	return output.String()
}

// Nominee links a GitLabUser with their number of Points
type Nominee struct {
	User   GitLabUser
	Points uint
	Index  uint // First appearance in a rank, used to make ties stable
}

// ByPoints implements sort.Interface based on the Points field.
type ByPoints []Nominee

// Sorting functions for ByPoints, used for ranking based on the number of points
func (p ByPoints) Len() int           { return len(p) }
func (p ByPoints) Less(i, j int) bool { return p[i].Points < p[j].Points }
func (p ByPoints) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
