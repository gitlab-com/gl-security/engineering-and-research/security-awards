package awards

import (
	"regexp"
	"strconv"
)

// GitLabURL defines fields for issues or merge requests
type GitLabURL string

// REAPIPath is the regular expression to extract parts of an issue or a merge request
var REAPIPath = regexp.MustCompile(`https:\/\/gitlab.com\/(gitlab-(org|com)\/.*)\/-\/(issues|merge_requests)\/(\d+)`)

// apiPath extracts the id of the issueable, and the encoded full path of the project
func (u GitLabURL) split() (projectPath, Type, ID string) {
	if match := REAPIPath.FindStringSubmatch(string(u)); len(match) > 0 {
		// [0]: full string
		// [1]: project path
		// [2]: org or com. Ignored
		// [3]: issues or merge_requests
		// [4]: ID
		return match[1], match[3], match[4]
	}
	return "", "", ""
}

// Type returns the type of GitLabURL ("issues" or "merge_requests")
func (u GitLabURL) Type() string {
	_, t, _ := u.split()
	return t
}

// ProjectPath returns the project path of the GitLabURL (ex: "gitlab-org/gitlab")
func (u GitLabURL) ProjectPath() string {
	pp, _, _ := u.split()
	return pp
}

// ID returns the ID of the GitLabURL
func (u GitLabURL) ID() int {
	_, _, id := u.split()
	s, _ := strconv.Atoi(id)
	return s
}

// IsAnIssue returns true if the GitLabURL is an issue
func (u GitLabURL) IsAnIssue() bool {
	_, t, _ := u.split()
	return t == "issues"
}
