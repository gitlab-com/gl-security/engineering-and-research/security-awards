package main

import (
	"errors"
	"testing"
	"time"

	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/gitlab"
)

type TestClient struct {
	team bool
}

func (tc TestClient) CouncilIssues() gitlab.CouncilIssuesManager {
	return &TestCouncilIssuesService{}
}

func (tc TestClient) Nominations() gitlab.NominationsManager {
	return &TestNominationsService{}
}

func (tc TestClient) SecurityMRs() gitlab.SecurityMRsManager {
	return &TestSecurityMRsService{}
}

func (tc TestClient) SetCredentials(APIToken, BaseURL string) error {
	if BaseURL == "" {
		return errors.New("SetCredentials error: BaseURL is empty")
	}

	if APIToken == "" {
		return errors.New("SetCredentials error: APIToken is empty")
	}
	return nil
}

func (tc *TestClient) LoadTeam(path string) error {
	tc.team = true
	return nil
}

type TestCouncilIssuesService struct{}

func (cis TestCouncilIssuesService) CreateIssue(int, string, string, []string) (*awards.CouncilIssue, error) {
	return &awards.CouncilIssue{WebURL: "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1"}, nil
}
func (cis TestCouncilIssuesService) CloseIssue(awards.CouncilIssue) error {
	return nil
}
func (cis TestCouncilIssuesService) CreateIssueDiscussion(awards.CouncilIssue, string) error {
	return nil
}
func (cis TestCouncilIssuesService) ListIssues(int) ([]awards.CouncilIssue, error) {
	return []awards.CouncilIssue{
		{
			WebURL: "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/123",
			Title:  "Security-Awards Council Discussion week of 2021-02-01",
			IID:    123,
		},
	}, nil
}
func (cis TestCouncilIssuesService) ListNominations(awards.CouncilIssue) ([]awards.Nomination, error) {
	return []awards.Nomination{
		{
			Category:        awards.CategoryEngineering,
			Title:           "Sign OAuth",
			WebURL:          "https://gitlab.com/gitlab-org/security/gitlab-pages/-/merge_requests/6",
			NominatedBy:     awards.GitLabUser("rchan-gitlab"),
			CouncilIssueURL: "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/123#note_1126",
		},
		{

			Category:        awards.CategoryCommunity,
			WebURL:          "https://gitlab.com/gitlab-org/security/gitaly/-/merge_requests/125",
			Title:           "Awesome security improvement",
			NominatedBy:     awards.GitLabUser("vdesousa"),
			CouncilIssueURL: "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/123#note_1127",
			ThumbsUP:        []awards.GitLabUser{"dappelt", "rchan-gitlab"},
		},
	}, nil
}

type TestNominationsService struct{}

func (ns TestNominationsService) GetNominations() (awards.Nominations, error) {
	return []awards.Nomination{
		{
			Title:       "A non-member can merge an MR to master",
			WebURL:      "https://gitlab.com/gitlab-org/gitlab/-/issues/290710",
			NominatedBy: awards.GitLabUser("rchan-gitlab"),
		},
		{
			Title:       "Forbid public",
			WebURL:      "https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1117",
			NominatedBy: awards.GitLabUser("rchan-gitlab"),
		},
		{
			Title:           "Sign OAuth",
			WebURL:          "https://gitlab.com/gitlab-org/security/gitlab-pages/-/merge_requests/6",
			NominatedBy:     awards.GitLabUser("rchan-gitlab"),
			CouncilIssueURL: "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/123#note_1126",
		},
		{
			Title:       "Fix issues",
			WebURL:      "https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1095",
			NominatedBy: awards.GitLabUser("rchan-gitlab"),
		},
		{
			Title:       "Replace umask usage with files permission change when a non-root image used",
			WebURL:      "https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2539",
			NominatedBy: awards.GitLabUser("rchan-gitlab"),
		},
		{
			Title:       "Add encrypted ldap secrets support",
			WebURL:      "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45712",
			NominatedBy: awards.GitLabUser("rchan-gitlab"),
		},
	}, nil
}
func (ns TestNominationsService) RejectNomination(awards.Nomination) error {
	return nil
}
func (ns TestNominationsService) AwardNomination(awards.Nomination, string) error {
	return nil
}

func TestSetCredential(t *testing.T) {
	t.Run("Should fail Without API token", func(t *testing.T) {
		err := TestClient{}.SetCredentials("", "https://gitlab.com")
		if err.Error() != "SetCredentials error: APIToken is empty" {
			t.Errorf("Fetch should fail if api-token is empty (err=%s)", err)
		}
	})

	t.Run("Should fail Without API Base URL", func(t *testing.T) {
		err := TestClient{}.SetCredentials("asdf", "")
		if err.Error() != "SetCredentials error: BaseURL is empty" {
			t.Errorf("Fetch should fail if api-baseurl is empty (err=%s)", err)
		}
	})

}

type TestSecurityMRsService struct{}

func (ns TestSecurityMRsService) ListMergeRequests(since time.Time) ([]awards.SecurityMR, error) {
	return []awards.SecurityMR{
		{
			Category: awards.CategoryEngineering,
			WebURL:   "https://gitlab.com/gitlab-org/security/gitaly/-/merge_requests/1565",
			Author:   awards.GitLabUser("igor.drozdov"),
			Points:   100,
			Severity: awards.Severity1,
			Reviewers: awards.Reviewers{
				Points: 50,
				Categories: map[awards.Category][]awards.GitLabUser{
					awards.CategoryEngineering: {"robotmay_gitlab", "pks-t"},
				},
			},
		},
	}, nil
}

// Check if client.LoadTeam() has been called
func isLoadTeamCalled(t *testing.T, client *TestClient) {
	if !client.team {
		t.Error("Team wasn't loaded, and is required for this command")
	}
}
