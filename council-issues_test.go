package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
)

func TestCreateCouncilIssue(t *testing.T) {
	t.Run("dry-run", func(t *testing.T) {

		os.Setenv("GITLAB_API_TOKEN", "asdf123")

		output := new(bytes.Buffer)
		app := NewApp(&TestClient{})
		app.Writer = output

		err := app.Run([]string{os.Args[0], "council-issues", "create", "--dry-run"})
		if err != nil {
			t.Fatal(err)
		}
		got := output.String()

		want := `Gathering nominations, please wait... [OK]
Generating issue with:
	[Project ID]: 4501968
	[Title]: Security-Awards Council Discussion week of ` + lastMonday().Format(awards.LayoutISO) + `
	[Confidential]: true
	[Labels]: [Security Awards Council]
	[Discussion 1]:		
		### [A non-member can merge an MR to master](https://gitlab.com/gitlab-org/gitlab/-/issues/290710)
		
		Nominated by @rchan-gitlab
	[Discussion 2]:		
		### [Forbid public](https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1117)
		
		Nominated by @rchan-gitlab
	[Discussion 3]:		
		### [Fix issues](https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1095)
		
		Nominated by @rchan-gitlab
	[Discussion 4]:		
		### [Replace umask usage with files permission change when a non-root image used](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2539)
		
		Nominated by @rchan-gitlab
	[Discussion 5]:		
		### [Add encrypted ldap secrets support](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45712)
		
		Nominated by @rchan-gitlab
Done (5 discussions)
`

		if diff := cmp.Diff(want, got); diff != "" {
			// full output
			fmt.Printf("got = %+v\n", got)

			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}
	})

	t.Run("no dry-run", func(t *testing.T) {
		os.Setenv("GITLAB_API_TOKEN", "asdf123")

		output := new(bytes.Buffer)
		app := NewApp(&TestClient{})
		app.Writer = output

		err := app.Run([]string{os.Args[0], "council-issues", "create"})
		if err != nil {
			t.Fatal(err)
		}
		got := output.String()

		want := `Gathering nominations, please wait... [OK]
Generating issue with:
	[Project ID]: 4501968
	[Title]: Security-Awards Council Discussion week of ` + lastMonday().Format(awards.LayoutISO) + `
	[Confidential]: true
	[Labels]: [Security Awards Council]
	[URL]: https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1
	[Discussion 1]:		
		### [A non-member can merge an MR to master](https://gitlab.com/gitlab-org/gitlab/-/issues/290710)
		
		Nominated by @rchan-gitlab
	[Discussion 2]:		
		### [Forbid public](https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1117)
		
		Nominated by @rchan-gitlab
	[Discussion 3]:		
		### [Fix issues](https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1095)
		
		Nominated by @rchan-gitlab
	[Discussion 4]:		
		### [Replace umask usage with files permission change when a non-root image used](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2539)
		
		Nominated by @rchan-gitlab
	[Discussion 5]:		
		### [Add encrypted ldap secrets support](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45712)
		
		Nominated by @rchan-gitlab
Done (5 discussions)
`

		if diff := cmp.Diff(want, got); diff != "" {
			// full output
			fmt.Printf("got = %+v\n", got)

			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}
	})
}

func TestCloseCouncilIssue(t *testing.T) {
	// Create temp file from the awardsYAML struct
	tmpAwardsFile := createTmpAwardsDataFile(t)
	defer os.Remove(tmpAwardsFile.Name()) // clean up

	os.Setenv("GITLAB_API_TOKEN", "asdf123")
	os.Setenv("AWARDS_FILE_PATH", tmpAwardsFile.Name())

	output := new(bytes.Buffer)
	client := &TestClient{}
	app := NewApp(client)
	app.Writer = output

	err := app.Run([]string{os.Args[0], "council-issues", "close"})
	if err != nil {
		t.Fatal(err)
	}

	isLoadTeamCalled(t, client)

	got := output.String()

	want := fmt.Sprintf(`Found 1 open council issue(s).

[Council Issue] Security-Awards Council Discussion week of 2021-02-01
Checking nomination for "Sign OAuth": Not enough votes [Rejected]
Checking nomination for "Awesome security improvement": [Awarded]

Writing data file (%s): [OK]
`, tmpAwardsFile.Name())

	if diff := cmp.Diff(want, got); diff != "" {
		// full output
		fmt.Printf("got = %+v\n", got)

		t.Errorf("Output mismatch (-want +got):\n%s", diff)
	}

	t.Run("Saved awards file", func(t *testing.T) {
		savedAwards, err := ioutil.ReadFile(tmpAwardsFile.Name())
		if err != nil {
			t.Fatal(err)
		}

		want = `- date: "2020-11-23"
  nomination:
    author: pks-t
    category: engineering
    council_issue_url: https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1089#note_447389715
    nominated_by: rchan-gitlab
    points: 200
    thumbs_up:
    - dappelt
    - plafoucriere
    web_url: https://gitlab.com/gitlab-org/gitaly/-/merge_requests/2669
- date: "2020-08-23"
  nomination:
    author: igor.drozdov
    category: engineering
    council_issue_url: https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1090#note_447389716
    nominated_by: dappelt
    points: 200
    thumbs_up:
    - rchan-gitlab
    - plafoucriere
    web_url: https://gitlab.com/gitlab-org/gitaly/-/merge_requests/2670
- date: "` + now().Format(awards.LayoutISO) + `"
  nomination:
    author: ""
    category: community
    council_issue_url: https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/123#note_1127
    nominated_by: vdesousa
    points: 200
    thumbs_up:
    - dappelt
    - rchan-gitlab
    web_url: https://gitlab.com/gitlab-org/security/gitaly/-/merge_requests/125
`

		if diff := cmp.Diff(want, string(savedAwards)); diff != "" {
			t.Errorf("Awards file content mismatch (-want +got):\n%s", diff)
		}
	})

}
