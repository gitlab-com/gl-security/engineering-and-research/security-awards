ARG GO_VERSION=1.16
FROM golang:$GO_VERSION-alpine AS builder

ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /src

COPY . .
RUN go build -v -ldflags="-s -w" -o /bin/awards

FROM busybox:stable
ENV AWARDS_FILE_PATH=/data/awards.yml \
    TEAM_FILE_PATH=/data/team.yml
COPY --from=builder /bin/awards /usr/local/bin/
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY data /data
ADD https://about.gitlab.com/company/team/team.yml /data/team.yml

ENTRYPOINT ["/usr/local/bin/awards"]
