package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"

	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
	"sigs.k8s.io/yaml"
)

func generateLeaderboardCmd(c *cli.Context) error {
	// load file
	b, err := ioutil.ReadFile(filepath.Clean(c.String(dataFileFlagName)))
	if err != nil {
		return err
	}

	aa := awards.Awards{}
	err = yaml.Unmarshal(b, &aa)
	if err != nil {
		return err
	}

	l := aa.ToLeaderBoard()

	_, err = fmt.Fprintln(c.App.Writer, l.ToMarkdown())

	return err
}
