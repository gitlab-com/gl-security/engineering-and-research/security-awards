package gitlab

import (
	"fmt"
	"net/url"
	"regexp"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
)

// CouncilIssuesService is a service to manage Council Issues
type CouncilIssuesService struct {
	client *gitlab.Client
	team   awards.GitLabTeam
}

// CreateIssue created a new confidential Council Issue
func (cis CouncilIssuesService) CreateIssue(pid int, title, desc string, labels []string) (*awards.CouncilIssue, error) {
	l := gitlab.Labels(labels)
	i := &gitlab.CreateIssueOptions{
		Title:        gitlab.String(title),
		Description:  gitlab.String(desc),
		Confidential: gitlab.Bool(true),
		Labels:       &l,
	}

	var issue *gitlab.Issue

	var response *gitlab.Response
	var err error
	issue, response, err = cis.client.Issues.CreateIssue(pid, i)
	if err != nil {
		return &awards.CouncilIssue{}, fmt.Errorf("CreateIssue error: %s (response: %+v)", err, response)
	}
	return &awards.CouncilIssue{
		Title:        issue.Title,
		WebURL:       awards.GitLabURL(issue.WebURL),
		IID:          issue.IID,
		Confidential: issue.Confidential,
	}, nil
}

// CreateIssueDiscussion creates a new issue discusion representing a Nomination
func (cis CouncilIssuesService) CreateIssueDiscussion(issue awards.CouncilIssue, body string) error {
	d := &gitlab.CreateIssueDiscussionOptions{
		Body: gitlab.String(body),
	}

	_, response, err := cis.client.Discussions.CreateIssueDiscussion(issue.WebURL.ProjectPath(), issue.IID, d)
	if err != nil {
		return fmt.Errorf("CreateDiscussion error: %s (response: %+v)", err, response)
	}
	return nil
}

// ListIssues returns the list of open Council Issues
func (cis CouncilIssuesService) ListIssues(pid int) ([]awards.CouncilIssue, error) {
	listOpts := &gitlab.ListProjectIssuesOptions{
		Labels:        &gitlab.Labels{"Security Awards Council"},
		State:         gitlab.String("opened"),
		Scope:         gitlab.String("all"),
		CreatedBefore: gitlab.Time(time.Now().AddDate(0, 0, -8)),
	}

	var issues []awards.CouncilIssue

	ii, response, err := cis.client.Issues.ListProjectIssues(pid, listOpts)
	if err != nil {
		return issues, fmt.Errorf("ListIssues error: %s (response: %+v)", err, response)
	}

	for _, issue := range ii {
		issues = append(issues, awards.CouncilIssue{
			Title:        issue.Title,
			WebURL:       awards.GitLabURL(issue.WebURL),
			IID:          issue.IID,
			Confidential: issue.Confidential,
		})
	}

	return issues, nil
}

// ListNominations returns the list of open Council Issues
func (cis CouncilIssuesService) ListNominations(issue awards.CouncilIssue) ([]awards.Nomination, error) {
	nominations := []awards.Nomination{}

	// Fetch discussions

	// regexp to parse dicussions
	re := regexp.MustCompile(`\[(?P<Title>.*)\]\((?P<URL>.*)\)\n\nNominated by @(?P<Nominated>.*)`)

	discussions, response, err := cis.client.Discussions.ListIssueDiscussions(issue.WebURL.ProjectPath(), issue.IID, &gitlab.ListIssueDiscussionsOptions{})
	if err != nil {
		return nominations, fmt.Errorf("ListNominations error: %s (response: %+v)", err, response)
	}

	for _, discussion := range discussions {

		note := discussion.Notes[0]
		res := re.FindStringSubmatch(note.Body)
		if len(res) != 4 {
			// No match, continue to next discussion
			continue
		}

		// Create direct link to discussion, for the awards VoteIssueURL
		iURL, err := url.Parse(string(issue.WebURL))
		if err != nil {
			return nominations, err
		}
		iURL.Fragment = fmt.Sprintf("note_%d", note.ID)

		newNomination := awards.Nomination{
			Title:           res[1],
			WebURL:          awards.GitLabURL(res[2]),
			NominatedBy:     awards.GitLabUser(res[3]),
			CouncilIssueURL: iURL.String(),
		}

		// fetch award emojis
		emojis, response, err := cis.client.AwardEmoji.ListIssuesAwardEmojiOnNote(issue.WebURL.ProjectPath(), issue.WebURL.ID(), note.ID, &gitlab.ListAwardEmojiOptions{})
		if err != nil {
			return nominations, fmt.Errorf("ListNominations error: %s (response: %+v)", err, response)
		}

		for _, emoji := range emojis {
			if strings.HasPrefix(emoji.Name, "thumbsup") {
				user := awards.GitLabUser(emoji.User.Username)
				member := cis.team.Find(user)
				if member != nil {
					if member.IsMemberOfSecurityDept() {
						newNomination.ThumbsUP = append(newNomination.ThumbsUP, user)
					}
				}
			}
		}

		// Fetch gitlab user to award (author of the Issue or Merge Request)
		if newNomination.WebURL.IsAnIssue() {
			i, r, err := cis.client.Issues.GetIssue(newNomination.WebURL.ProjectPath(), newNomination.WebURL.ID())
			if err != nil {
				return nominations, fmt.Errorf("ListNominations error: %s (response: %+v)", err, r)
			}
			newNomination.Author = awards.GitLabUser(i.Author.Username)
		} else {
			mr, r, err := cis.client.MergeRequests.GetMergeRequest(newNomination.WebURL.ProjectPath(), newNomination.WebURL.ID(), &gitlab.GetMergeRequestsOptions{})
			if err != nil {
				return nominations, fmt.Errorf("ListNominations error: %s (response: %+v)", err, r)
			}
			newNomination.Author = awards.GitLabUser(mr.Author.Username)
		}

		// Find the right category
		newNomination.Category = cis.team.UserCategory(newNomination.Author)

		nominations = append(nominations, newNomination)
	}
	return nominations, nil
}

// CloseIssue closes a Council issue
func (cis CouncilIssuesService) CloseIssue(issue awards.CouncilIssue) error {
	_, response, err := cis.client.Issues.UpdateIssue(issue.WebURL.ProjectPath(), issue.IID, &gitlab.UpdateIssueOptions{StateEvent: gitlab.String("close")})
	if err != nil {
		return fmt.Errorf("Error while closing issue %q: %s (response: %+v)", issue.Title, err, response)
	}
	return nil
}
