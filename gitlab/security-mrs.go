package gitlab

import (
	"strconv"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
)

// SecurityMRsService is a service to manage Security Merge Requests
type SecurityMRsService struct {
	client *gitlab.Client
	gid    string // Group ID
	team   awards.GitLabTeam
}

const securityLabel = "security"

// ListMergeRequests List Security Merge Requests in `gitlab-org/security`
func (smr SecurityMRsService) ListMergeRequests(since time.Time) ([]awards.SecurityMR, error) {
	securityMRs := []awards.SecurityMR{}

	page := 1
	mergeRequests := []*gitlab.MergeRequest{}

	for page != 0 {
		listOpts := &gitlab.ListGroupMergeRequestsOptions{
			CreatedAfter: gitlab.Time(since),
			Labels:       &gitlab.Labels{securityLabel},
			Scope:        gitlab.String("all"), // By default it returns only merge requests created by the current user. To get all merge requests, use parameter scope=all.
			State:        gitlab.String("merged"),
			TargetBranch: gitlab.String("master"),
			ListOptions: gitlab.ListOptions{
				Page:    page,
				PerPage: 100,
			},
		}

		mergeRequestsPage, response, err := smr.client.MergeRequests.ListGroupMergeRequests(smr.gid, listOpts)
		if err != nil {
			return securityMRs, err
		}

		page = response.NextPage
		mergeRequests = append(mergeRequests, mergeRequestsPage...)
	}

	getCategory := func(username string) awards.Category { return smr.team.UserCategory(awards.GitLabUser(username)) }
	canBeRewarded := func(username string) bool {
		// Users can be rewarded if they're part of the company (in team.yml) and are _not_ members of the Security Department
		if u := smr.team.Find(awards.GitLabUser(username)); u != nil {
			return !u.IsMemberOfSecurityDept()
		}
		return false
	}

	for _, mr := range mergeRequests {
		severity := severityFromLabels(mr.Labels)
		if severity == 0 {
			// No severity label, proceed to next merge request
			continue
		}

		if !canBeRewarded(mr.Author.Username) {
			// Skip the MR altogether
			continue
		}

		newSMR := awards.SecurityMR{
			WebURL:   awards.GitLabURL(mr.WebURL),
			Severity: severityFromLabels(mr.Labels),
			Author:   awards.GitLabUser(mr.Author.Username),
			Category: getCategory(mr.Author.Username),
			Points:   uint(100 - (severity-1)*20),
			Reviewers: awards.Reviewers{
				Categories: map[awards.Category][]awards.GitLabUser{},
				Points:     uint(50 - (severity-1)*10),
			},
		}

		for _, reviewer := range mr.Reviewers {
			// Members of the Security Department can't be rewarded
			if canBeRewarded(reviewer.Username) {
				cat := newSMR.Reviewers.Categories[getCategory(reviewer.Username)]
				cat = append(cat, awards.GitLabUser(reviewer.Username))
				newSMR.Reviewers.Categories[getCategory(reviewer.Username)] = cat
			}
		}

		securityMRs = append(securityMRs, newSMR)
	}

	return securityMRs, nil
}

func severityFromLabels(labels gitlab.Labels) awards.Severity {
	var severity awards.Severity // If not severity is found, 0 will be returned, resulting a non-valid award.SecurityMR
	for _, l := range labels {
		parts := strings.Split(l, "severity::")
		if len(parts) == 2 {
			s, _ := strconv.Atoi(parts[1])
			severity = awards.Severity(s)
		}
	}
	return severity
}
