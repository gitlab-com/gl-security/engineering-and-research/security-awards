package gitlab

import (
	"errors"
	"time"

	gogitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
)

const (
	// LabelNomination is the label name for nominations
	LabelNomination = "security-awards::nomination"
	// LabelAwarded if the label name for awarded actions
	LabelAwarded = "security-awards::awarded"
)

// Client is standard client interface so other types of clients can be provided (for testing)
type Client interface {
	SetCredentials(string, string) error
	LoadTeam(string) error
	CouncilIssues() CouncilIssuesManager
	Nominations() NominationsManager
	SecurityMRs() SecurityMRsManager
}

// TokenClient is an implementation of the Client interface, holding a private API token
type TokenClient struct {
	*gogitlab.Client
	councilIssues *CouncilIssuesService
	nominations   *NominationsService
	securityMRs   *SecurityMRsService
}

// SetCredentials stores the api private token, and the gitlab instance base URL ("/api/v4" will be appended).
func (c *TokenClient) SetCredentials(APIToken, BaseURL string) error {
	if BaseURL == "" {
		return errors.New("SetCredentials error: APIBaseURL is empty")
	}

	if APIToken == "" {
		return errors.New("SetCredentials error: APIToken is empty")
	}

	var err error
	// Create the underlying go-gitlab client
	c.Client, err = gogitlab.NewClient(APIToken, gogitlab.WithBaseURL(BaseURL))

	// Assign the client to all services
	c.councilIssues = &CouncilIssuesService{client: c.Client}
	c.nominations = &NominationsService{client: c.Client, token: APIToken}
	c.securityMRs = &SecurityMRsService{client: c.Client, gid: "gitlab-org/security"}
	return err
}

// LoadTeam is loading the GitLab team file from the path provided, and store it in the underlying structs
func (c *TokenClient) LoadTeam(path string) error {
	t, err := awards.LoadTeam(path)
	if err != nil {
		return err
	}
	c.securityMRs.team = t
	c.councilIssues.team = t
	return nil
}

// Nominations returns the client NominationsService
func (c *TokenClient) Nominations() NominationsManager {
	return c.nominations
}

// CouncilIssues returns the client CouncilIssuesService
func (c *TokenClient) CouncilIssues() CouncilIssuesManager {
	return c.councilIssues
}

// SecurityMRs returns the client CouncilIssuesService
func (c *TokenClient) SecurityMRs() SecurityMRsManager {
	return c.securityMRs
}

// CouncilIssuesManager is a CouncilIssue manager interface
type CouncilIssuesManager interface {
	CreateIssue(int, string, string, []string) (*awards.CouncilIssue, error)
	CloseIssue(awards.CouncilIssue) error
	CreateIssueDiscussion(awards.CouncilIssue, string) error
	ListIssues(int) ([]awards.CouncilIssue, error)
	ListNominations(awards.CouncilIssue) ([]awards.Nomination, error)
}

// NominationsManager is a Nominations manager interface
type NominationsManager interface {
	GetNominations() (awards.Nominations, error)
	RejectNomination(awards.Nomination) error
	AwardNomination(awards.Nomination, string) error
}

// SecurityMRsManager is a SecurityMRs manager interface
type SecurityMRsManager interface {
	ListMergeRequests(time.Time) ([]awards.SecurityMR, error)
}
