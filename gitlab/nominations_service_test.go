package gitlab

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
)

func TestNominationService(t *testing.T) {

	t.Run("GetNominations", func(t *testing.T) {
		mux, ts := testServer(t)
		defer ts.Close()

		client := TokenClient{}
		if err := client.SetCredentials("asdf123", ts.URL); err != nil {
			t.Fatal(err)
		}

		// issues and MRs with the label security-awards::nomination
		mux.HandleFunc("/api/graphql", func(w http.ResponseWriter, r *http.Request) {
			jsonResponse := `
{
   "data" : {
      "council_issues" : {
         "issues" : {
            "nodes" : [
               {
                  "discussions" : {
                     "nodes" : [
                        {
                           "notes" : {
                              "nodes" : [
                                 {
                                    "body" : "### [Sign OAuth](https://gitlab.com/gitlab-org/security/gitlab-pages/-/merge_requests/6)\n\nNominated by @dappelt"
                                 }
                              ]
                           }
                        }
                     ]
                  },
                  "state" : "opened",
                  "title" : "Security-Awards Council Discussion week of 2021-01-11",
                  "webUrl" : "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/123"
               }
            ]
         },
         "name" : "Security Department Meta Project"
      },
      "gitlab_com" : {
         "issues" : {
            "nodes" : []
         },
         "mergeRequests" : {
            "nodes" : []
         }
      },
      "gitlab_org" : {
         "issues" : {
            "nodes" : [
               {
                  "state" : "closed",
                  "title" : "A non-member can merge an MR to master",
                  "webUrl" : "https://gitlab.com/gitlab-org/gitlab/-/issues/290710"
               }
            ]
         },
         "mergeRequests" : {
            "nodes" : [
               {
                  "state" : "opened",
                  "title" : "Forbid public",
                  "webUrl" : "https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1117"
               },
               {
                  "state" : "opened",
                  "title" : "Sign OAuth",
                  "webUrl" : "https://gitlab.com/gitlab-org/security/gitlab-pages/-/merge_requests/6"
               },
               {
                  "state" : "opened",
                  "title" : "Fix issues",
                  "webUrl" : "https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1095"
               },
               {
                  "state" : "merged",
                  "title" : "Replace umask usage with files permission change when a non-root image used",
                  "webUrl" : "https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2539"
               },
               {
                  "state" : "merged",
                  "title" : "Add encrypted ldap secrets support",
                  "webUrl" : "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45712"
               }
            ]
         }
      }
   }
}
`

			// Resource Label events
			// Example URL: /api/v4/projects/gitlab-org%2Fgitlab/issues/290710/resource_label_events?per_page=100
			mux.HandleFunc("/api/v4/projects/{project_path}/{resource[(issues|merge_requests)]}/{id:[0-9]+}/resource_label_events", func(w http.ResponseWriter, r *http.Request) {
				q := r.URL.Query()

				if diff := cmp.Diff(q["per_page"], []string{"100"}); diff != "" {
					t.Errorf("Target Branch mismatch (-want, +got): %s", diff)
				}

				w.Header().Set("x-per-page", "100")
				w.Header().Set("x-total-pages", "2")

				// Simulate pagination
				switch page := q.Get("page"); page {
				case "1":
					w.Header().Set("x-next-page", "2")
					w.Header().Set("x-page", "1")

					fmt.Fprintln(w, `
[
   {
      "action" : "remove",
      "created_at" : "2020-12-16T12:54:00.580Z",
      "id" : 78057471,
      "label" : {
         "color" : "#ED6C28",
         "description" : "Issues for the Package stage of the DevOps lifecycle (e.g. Container Registry, Dependency Proxy)",
         "description_html" : "Issues for the Package stage of the DevOps lifecycle (e.g. Container Registry, Dependency Proxy)",
         "id" : 3103454,
         "name" : "devops::package",
         "text_color" : "#FFFFFF"
      },
      "resource_id" : 75703233,
      "resource_type" : "Issue",
      "user" : {
         "avatar_url" : "https://secure.gravatar.com/avatar/73912e9701e47bb21b40181ec811eb45?s=80&d=identicon",
         "id" : 423116,
         "name" : "Markus Koller",
         "state" : "active",
         "username" : "toupeira",
         "web_url" : "https://gitlab.com/toupeira"
      }
   },
   {
      "action" : "remove",
      "created_at" : "2020-12-16T12:54:00.580Z",
      "id" : 78057472,
      "label" : {
         "color" : "#A8D695",
         "description" : "Issues belonging to the Package group of the Package stage of the DevOps lifecycle. See https://about.gitlab.com/handbook/product/categories/#package-group",
         "description_html" : "Issues belonging to the Package group of the Package stage of the DevOps lifecycle. See <a href=\"https://about.gitlab.com/handbook/product/categories/#package-group\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://about.gitlab.com/handbook/product/categories/#package-group</a>",
         "id" : 10690711,
         "name" : "group::package",
         "text_color" : "#333333"
      },
      "resource_id" : 75703233,
      "resource_type" : "Issue",
      "user" : {
         "avatar_url" : "https://secure.gravatar.com/avatar/73912e9701e47bb21b40181ec811eb45?s=80&d=identicon",
         "id" : 423116,
         "name" : "Markus Koller",
         "state" : "active",
         "username" : "toupeira",
         "web_url" : "https://gitlab.com/toupeira"
      }
   }
]`)

				case "2":
					w.Header().Set("x-page", "2")
					fmt.Fprintf(w, `
[
   {
      "action" : "remove",
      "created_at" : "2020-12-16T12:54:00.580Z",
      "id" : 78057473,
      "label" : {
         "color" : "#F0AD4E",
         "description" : "Any issues related to the Ops Section",
         "description_html" : "Any issues related to the Ops Section",
         "id" : 13979951,
         "name" : "section::ops",
         "text_color" : "#FFFFFF"
      },
      "resource_id" : 75703233,
      "resource_type" : "Issue",
      "user" : {
         "avatar_url" : "https://secure.gravatar.com/avatar/73912e9701e47bb21b40181ec811eb45?s=80&d=identicon",
         "id" : 423116,
         "name" : "Markus Koller",
         "state" : "active",
         "username" : "toupeira",
         "web_url" : "https://gitlab.com/toupeira"
      }
   },
   {
      "action" : "add",
      "created_at" : "2021-01-10T23:19:23.370Z",
      "id" : 80947872,
      "label" : {
         "color" : "#DAA520",
         "description" : "Issue or Merge Request nominated for a security award. See https://about.gitlab.com/handbook/security/security-awards-program.html",
         "description_html" : "Issue or Merge Request nominated for a security award. See <a href=\"https://about.gitlab.com/handbook/security/security-awards-program.html\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://about.gitlab.com/handbook/engineering/security/security-awards-program.html</a>",
         "id" : 16708504,
         "name" : "security-awards::nomination",
         "text_color" : "#FFFFFF"
      },
      "resource_id" : 75703233,
      "resource_type" : "Issue",
      "user" : {
         "avatar_url" : "https://assets.gitlab-static.net/uploads/-/system/user/avatar/5616676/avatar.png",
         "id" : 5616676,
         "name" : "Ron Chan",
         "state" : "active",
         "username" : "rchan-gitlab",
         "web_url" : "https://gitlab.com/rchan-gitlab"
      }
   }
]
`)

				default:
					t.Errorf("Unexpected page: %v", page)
				}

			}).Methods(http.MethodGet)

			fmt.Fprintln(w, jsonResponse)
		}).Methods(http.MethodPost)

		got, err := client.nominations.GetNominations()
		if err != nil {
			t.Fatal(err)
		}

		want := []awards.Nomination{
			{
				Title:       "A non-member can merge an MR to master",
				WebURL:      "https://gitlab.com/gitlab-org/gitlab/-/issues/290710",
				NominatedBy: awards.GitLabUser("rchan-gitlab"),
			},
			{
				Title:       "Forbid public",
				WebURL:      "https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1117",
				NominatedBy: awards.GitLabUser("rchan-gitlab"),
			},
			{
				Title:       "Sign OAuth",
				WebURL:      "https://gitlab.com/gitlab-org/security/gitlab-pages/-/merge_requests/6",
				NominatedBy: awards.GitLabUser("rchan-gitlab"),
			},
			{
				Title:       "Fix issues",
				WebURL:      "https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1095",
				NominatedBy: awards.GitLabUser("rchan-gitlab"),
			},
			{
				Title:       "Replace umask usage with files permission change when a non-root image used",
				WebURL:      "https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2539",
				NominatedBy: awards.GitLabUser("rchan-gitlab"),
			},
			{
				Title:       "Add encrypted ldap secrets support",
				WebURL:      "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45712",
				NominatedBy: awards.GitLabUser("rchan-gitlab"),
			},
		}

		if diff := cmp.Diff(awards.Nominations(want), got); diff != "" {
			// full output
			fmt.Printf("got = %+v\n", got)

			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}

	})

	t.Run("RejectNomination", func(t *testing.T) {
		mux, ts := testServer(t)
		defer ts.Close()

		client := TokenClient{}
		if err := client.SetCredentials("asdf123", ts.URL); err != nil {
			t.Fatal(err)
		}

		mux.HandleFunc("/api/v4/projects/{project_path}/{resource[(issues|merge_requests)]}/{id:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {

			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Printf("Error reading body: %v", err)
				http.Error(w, "can't read body", http.StatusBadRequest)
				return
			}

			want := `{"remove_labels":"security-awards::awarded"}`
			if diff := cmp.Diff(want, string(body)); diff != "" {
				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}
			fmt.Fprint(w, `
{
   "author" : {
      "avatar_url" : null,
      "id" : 1,
      "name" : "Andy Volpe",
      "state" : "active",
      "username" : "andyvolpe",
      "web_url" : "https://gitlab.example.com/admin"
   },
   "id" : 1122334455,
   "iid" : 125,
   "project_id" : 3
}
`)
		}).Methods(http.MethodGet, http.MethodPut)

		t.Run("with Issue nomination", func(t *testing.T) {
			nomination := awards.Nomination{
				WebURL: "https://gitlab.com/gitlab-org/gitlab/-/issues/123",
			}

			err := client.Nominations().RejectNomination(nomination)
			if err != nil {
				t.Fatal(err)
			}
		})

		t.Run("Reject MR nomination", func(t *testing.T) {
			nomination := awards.Nomination{
				WebURL: "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/125",
			}

			err := client.Nominations().RejectNomination(nomination)
			if err != nil {
				t.Fatal(err)
			}
		})
	})

	t.Run("AwardNomination", func(t *testing.T) {
		mux, ts := testServer(t)
		defer ts.Close()

		client := TokenClient{}
		if err := client.SetCredentials("asdf123", ts.URL); err != nil {
			t.Fatal(err)
		}

		mux.HandleFunc("/api/v4/projects/{project_path}/{resource[(issues|merge_requests)]}/{id:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {

			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Printf("Error reading body: %v", err)
				http.Error(w, "can't read body", http.StatusBadRequest)
				return
			}

			want := `{"add_labels":"security-awards::awarded"}`
			if diff := cmp.Diff(want, string(body)); diff != "" {
				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}
			fmt.Fprint(w, `
{
  "id": 1122334455,
  "iid": 125,
  "project_id": 3,
  "author": {
    "id": 1,
    "name": "Andy Volpe",
    "username": "andyvolpe",
    "state": "active",
    "avatar_url": null,
    "web_url": "https://gitlab.example.com/admin"
  }
}
`)
		}).Methods(http.MethodGet, http.MethodPut)

		// Send congratulations
		mux.HandleFunc("/api/v4/projects/{project_path}/{resource[(issues|merge_requests)]}/{id:[0-9]+}/notes", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, `{"id":1}`)
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Printf("Error reading body: %v", err)
				http.Error(w, "can't read body", http.StatusBadRequest)
				return
			}

			want := `{"body":"congrats!"}`
			if diff := cmp.Diff(want, string(body)); diff != "" {
				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}
		}).Methods(http.MethodPost)

		t.Run("Award issue nomination", func(t *testing.T) {
			nomination := awards.Nomination{WebURL: "https://gitlab.com/gitlab-org/gitlab/-/issues/123"}

			err := client.Nominations().AwardNomination(nomination, "congrats!")
			if err != nil {
				t.Fatal(err)
			}
		})
		t.Run("Award MR nomination", func(t *testing.T) {
			nomination := awards.Nomination{WebURL: "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/125"}

			err := client.Nominations().AwardNomination(nomination, "congrats!")
			if err != nil {
				t.Fatal(err)
			}
		})
	})

}
