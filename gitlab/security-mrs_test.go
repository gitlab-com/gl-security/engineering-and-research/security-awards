package gitlab

import (
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
)

func TestSecurityMRsService(t *testing.T) {

	mux, ts := testServer(t)
	defer ts.Close()

	// Create teams.yml temp file
	tmpTeamFile := createTmpTeamDataFile(t)
	defer os.Remove(tmpTeamFile.Name()) // clean up

	client := TokenClient{}
	if err := client.SetCredentials("asdf123", ts.URL); err != nil {
		t.Fatal(err)
	}
	if err := client.LoadTeam(tmpTeamFile.Name()); err != nil {
		t.Fatal(err)
	}

	now := time.Now()

	t.Run("ListMergeRequests", func(t *testing.T) {

		mux.HandleFunc("/api/v4/groups/gitlab-org%2Fsecurity/merge_requests", func(w http.ResponseWriter, r *http.Request) {
			q := r.URL.Query()

			if diff := cmp.Diff(q["labels"], []string{"security"}); diff != "" {
				t.Errorf("Labels mismatch (-want, +got): %s", diff)
			}

			if diff := cmp.Diff(q["state"], []string{"merged"}); diff != "" {
				t.Errorf("State mismatch (-want, +got): %s", diff)
			}

			if diff := cmp.Diff(q["scope"], []string{"all"}); diff != "" {
				t.Errorf("Scope mismatch (-want, +got): %s", diff)
			}

			if diff := cmp.Diff(q["target_branch"], []string{"master"}); diff != "" {
				t.Errorf("Target Branch mismatch (-want, +got): %s", diff)
			}

			if diff := cmp.Diff(q["created_after"], []string{now.Format(time.RFC3339)}); diff != "" {
				t.Errorf("Target Branch mismatch (-want, +got): %s", diff)
			}

			if diff := cmp.Diff(q["per_page"], []string{"100"}); diff != "" {
				t.Errorf("Target Branch mismatch (-want, +got): %s", diff)
			}

			w.Header().Set("x-per-page", "100")
			w.Header().Set("x-total-pages", "2")

			// Simulate pagination
			switch page := q.Get("page"); page {
			case "1":
				w.Header().Set("x-next-page", "2")
				w.Header().Set("x-page", "1")

				fmt.Fprintln(w, `
[
   {
      "author" : {
         "id" : 1,
         "name" : "Andy Volpe",
         "username" : "andyvolpe"
      },
      "id" : 1122334455,
      "iid" : 125,
      "labels" : [
         "backend",
         "bug",
         "workflow::production",
         "severity::2",
         "security"
      ],
      "project_id" : 3,
      "reviewers" : [
         {
            "avatar_url" : "https://assets.gitlab-static.net/uploads/-/system/user/avatar/2167502/avatar.png",
            "id" : 2167502,
            "name" : "Robert May",
            "state" : "active",
            "username" : "robotmay_gitlab",
            "web_url" : "https://gitlab.com/robotmay_gitlab"
         },
         {
            "avatar_url" : "https://assets.gitlab-static.net/uploads/-/system/user/avatar/2880930/avatar.png",
            "id" : 2880930,
            "name" : "Heinrich Lee Yu",
            "state" : "active",
            "username" : "engwan",
            "web_url" : "https://gitlab.com/engwan"
         },
         {
            "avatar_url" : "https://assets.gitlab-static.net/uploads/-/system/user/avatar/2885565/avatar.png",
            "id" : 2880931,
            "name" : "Dennis Appelt",
            "state" : "active",
            "username" : "dappelt",
            "web_url" : "https://gitlab.com/dappelt"
         }
      ],
      "web_url" : "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/57109"
   },
   {
      "author" : {
         "id" : 1,
         "name" : "Andy Volpe",
         "username" : "andyvolpe"
      },
      "id" : 112233445566,
      "iid" : 126,
      "labels" : [
         "backend",
         "bug",
         "workflow::production",
         "security"
      ],
      "project_id" : 3,
      "reviewers" : [],
      "web_url" : "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/57110#will-be-ignored-because-no-severity"
   },
   {
      "author" : {
         "id" : 2,
         "name" : "Dennis Appelt",
         "username" : "dappelt"
      },
      "id" : 112233445577,
      "iid" : 127,
      "labels" : [
         "backend",
         "bug",
         "workflow::production",
         "security",
         "severity::2"
      ],
      "project_id" : 3,
      "reviewers" : [],
      "web_url" : "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/57110#will-be-ignored-because-author-is-a-security-engineer"
   },
   {
      "author" : {
         "id" : 3,
         "name" : "Issac Left",
         "username" : "ileft"
      },
      "id" : 112233445578,
      "iid" : 128,
      "labels" : [
         "backend",
         "bug",
         "workflow::production",
         "security",
         "severity::2"
      ],
      "project_id" : 3,
      "reviewers" : [],
      "web_url" : "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/57111#will-be-ignored-because-author-has-left-gitlab"
   }
]`)

			case "2":
				w.Header().Set("x-page", "2")
				fmt.Fprintf(w, `
[
   {
      "author" : {
         "id" : 2,
         "name" : "Heinrich Lee Yu",
         "username" : "engwan"
      },
      "id" : 112233445577,
      "iid" : 127,
      "labels" : [
         "backend",
         "bug",
         "workflow::production",
         "severity::1",
         "security"
      ],
      "project_id" : 4,
      "reviewers" : [
         {
            "avatar_url" : "https://assets.gitlab-static.net/uploads/-/system/user/avatar/2167502/avatar.png",
            "id" : 2167502,
            "name" : "Robert May",
            "state" : "active",
            "username" : "robotmay_gitlab",
            "web_url" : "https://gitlab.com/robotmay_gitlab"
         }
      ],
      "web_url" : "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/56956"
   }
]`)

			default:
				t.Errorf("Unexpected page: %v", page)
			}

		}).Methods(http.MethodGet)

		want := []awards.SecurityMR{
			{
				WebURL:   "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/57109",
				Severity: awards.Severity2,
				Author:   "andyvolpe",
				Category: awards.CategoryEngineering,
				Points:   80,
				Reviewers: awards.Reviewers{
					Categories: map[awards.Category][]awards.GitLabUser{
						awards.CategoryDev: []awards.GitLabUser{"robotmay_gitlab", "engwan"},
					},
					Points: 40,
				},
			},
			// the second MR is ignored because it doesn't have any severity label
			{
				WebURL:   "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/56956",
				Severity: awards.Severity1,
				Author:   "engwan",
				Category: awards.CategoryDev,
				Points:   100,
				Reviewers: awards.Reviewers{
					Categories: map[awards.Category][]awards.GitLabUser{
						awards.CategoryDev: []awards.GitLabUser{"robotmay_gitlab"},
					},
					Points: 50,
				},
			},
		}

		got, err := client.securityMRs.ListMergeRequests(now)
		if err != nil {
			t.Fatal(err)
		}

		if diff := cmp.Diff(want, got); diff != "" {
			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}

	})
}
