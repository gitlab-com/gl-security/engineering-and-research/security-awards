package gitlab

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
	"time"

	gitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
)

// NominationsService is a service to manage Nominations
type NominationsService struct {
	client *gitlab.Client
	token  string // Keep until the graphQL call in GetNominations is replaced
}

// GetNominations returns the list of active nominations (from the gitlab-org and gitlab-com namespaces)
func (ns NominationsService) GetNominations() (awards.Nominations, error) {
	// TODO: Replace by rest calls, using the gitlab client
	nominations := []awards.Nomination{}

	// Select only issues closed in the last 6 months, and MRs merged in the last 6 months.
	closedAfter := time.Now().AddDate(0, -6, 0).Format(awards.LayoutISO)
	mergedAfter := time.Now().AddDate(0, -6, 0).Format(awards.LayoutISO)

	jsonData := map[string]string{
		"query": `
		query Nominations{
			gitlab_org: group(fullPath: "gitlab-org") {
				...issuesAndMRs
			}
			gitlab_com: group(fullPath: "gitlab-com") {
				...issuesAndMRs
			}
			council_issues: project(fullPath: "gitlab-com/gl-security/security-department-meta") {
				name
				issues(labelName: "Security Awards Council") {
					nodes {
						title
						webUrl
						state
						discussions {
							nodes {
								notes {
									nodes {
										url
										body
									}
								}
							}
						}
					}
				}
			}
		}

		fragment issuesAndMRs on Group {
			issues(labelName: "` + LabelNomination + `", state: all, includeSubgroups: true, closedAfter: "` + closedAfter + `") {
				nodes {
					title
					webUrl
					state
				}
			}
			mergeRequests(labels: "` + LabelNomination + `", state: all, includeSubgroups: true, mergedAfter: "` + mergedAfter + `") {
				nodes {
					title
					webUrl
					state
				}
			}
		}`}

	type nodes struct {
		Nodes []struct {
			WebURL string
			Title  string
		}
	}

	type issuesAndMRs struct {
		Issues        nodes
		MergeRequests nodes
	}

	// councilIssues are used to figure out what nominations are new
	type councilIssues struct {
		Issues struct {
			Nodes []struct {
				*awards.GitLabURL
				Discussions struct {
					Nodes []struct {
						Notes struct {
							Nodes []struct {
								URL  string
								Body string
							}
						}
					}
				}
			}
		}
	}

	type dataStruct struct {
		Data struct {
			GitLabOrg     issuesAndMRs  `json:"gitlab_org"`
			GitLabCom     issuesAndMRs  `json:"gitlab_com"`
			CouncilIssues councilIssues `json:"council_issues"`
		}
	}

	// Send JSON request to the GitLab GraphQL API server
	postData, _ := json.Marshal(jsonData)

	baseURL := ns.client.BaseURL().String()
	baseURL = strings.Replace(baseURL, "/api/v4/", "/api/graphql", -1)

	request, err := http.NewRequest("POST", baseURL, bytes.NewBuffer(postData))
	if err != nil {
		return nominations, err
	}
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", ns.token))

	client := &http.Client{Timeout: time.Second * 60}
	response, err := client.Do(request)
	if err != nil {
		return nominations, err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return nominations, fmt.Errorf("Got non 200 status: %d", response.StatusCode)
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nominations, err
	}

	var d dataStruct

	err = json.Unmarshal(body, &d)
	if err != nil {
		return nominations, fmt.Errorf("Fetch nominations error: %s (response: %+v)", err, response)
	}

	data := d.Data

	// Flatten all nodes (MRs and Issues) from both gitlab-org and gitlab-com subprojects
	var n nodes
	n.Nodes = append(n.Nodes, data.GitLabOrg.Issues.Nodes...)
	n.Nodes = append(n.Nodes, data.GitLabOrg.MergeRequests.Nodes...)
	n.Nodes = append(n.Nodes, data.GitLabCom.Issues.Nodes...)
	n.Nodes = append(n.Nodes, data.GitLabCom.MergeRequests.Nodes...)

	// Expression to extract nomination links from council issues notes body
	re := regexp.MustCompile(`^\n?### \[.*\]\((.*)\)\n.*`)

	// Loop on all nodes to see if they are referenced in the Council Issues already
	for _, node := range n.Nodes {
		nomination := awards.Nomination{WebURL: awards.GitLabURL(node.WebURL), Title: node.Title}

		err = ns.getNominatedBy(&nomination)
		if err != nil {
			return nominations, err
		}

		for _, councilIssue := range data.CouncilIssues.Issues.Nodes {
			for _, discussion := range councilIssue.Discussions.Nodes {
				for _, note := range discussion.Notes.Nodes {
					var noteNomitationURL string

					// Try to capture the nomination url in the note from the current councilIssue
					if match := re.FindStringSubmatch(note.Body); len(match) > 0 {
						noteNomitationURL = match[1]
					}
					// If the captured URL is matching the nomination URL, link the current councilIssue to the nomination
					if node.WebURL == noteNomitationURL {
						nomination.CouncilIssueURL = note.URL
					}
				}
			}
		}
		nominations = append(nominations, nomination)
	}
	return nominations, nil
}

func (ns NominationsService) getNominatedBy(n *awards.Nomination) error {
	page := 1
	events := []*gitlab.LabelEvent{}

	for page != 0 {

		opts := &gitlab.ListLabelEventsOptions{
			ListOptions: gitlab.ListOptions{
				Page:    page,
				PerPage: 100,
			},
		}

		f := ns.ListLabelEventsFunc(n)
		eventsPage, response, err := f(n.WebURL.ProjectPath(), n.WebURL.ID(), opts)
		if err != nil {
			return fmt.Errorf("getNominatedBy error: %s", err)
		}
		page = response.NextPage
		events = append(events, eventsPage...)
	}

	// The list is order by date, so by not breaking the loop, we'll get the most recent one.
	for _, e := range events {
		// Don't use the label ID, as it can differ from gitlab-org to gitlab-com. Use Name instead.
		if e.Label.Name == LabelNomination && e.Action == "add" {
			n.NominatedBy = awards.GitLabUser(e.User.Username)
		}
	}

	return nil
}

// ListLabelEventsFunc returns the corresponding LabelEvents function, depending on the type of nomination
func (ns NominationsService) ListLabelEventsFunc(nomination *awards.Nomination) func(interface{}, int, *gitlab.ListLabelEventsOptions, ...gitlab.RequestOptionFunc) ([]*gitlab.LabelEvent, *gitlab.Response, error) {
	if nomination.WebURL.IsAnIssue() {
		return ns.client.ResourceLabelEvents.ListIssueLabelEvents
	}
	return ns.client.ResourceLabelEvents.ListMergeRequestsLabelEvents
}

// RejectNomination removes the ~security-awards::awarded label from a nomination
func (ns NominationsService) RejectNomination(nomination awards.Nomination) error {
	return ns.removeLabel(nomination, LabelAwarded)
}

// AwardNomination applies the ~security-awards::awarded label to a nomination, and add a comment if congratsMessage is not empty
func (ns NominationsService) AwardNomination(nomination awards.Nomination, congratsMessage string) error {
	err := ns.applyLabel(nomination, LabelAwarded)
	if err != nil {
		return err
	}

	// Send Congrats
	if congratsMessage != "" {
		var response *gitlab.Response
		var err error
		if nomination.WebURL.IsAnIssue() {
			_, response, err = ns.client.Notes.CreateIssueNote(nomination.WebURL.ProjectPath(), nomination.WebURL.ID(), &gitlab.CreateIssueNoteOptions{Body: gitlab.String(congratsMessage)})
		} else {
			_, response, err = ns.client.Notes.CreateMergeRequestNote(nomination.WebURL.ProjectPath(), nomination.WebURL.ID(), &gitlab.CreateMergeRequestNoteOptions{Body: gitlab.String(congratsMessage)})
		}
		if err != nil {
			return fmt.Errorf("Error while sending congrats on %q: %s (response: %+v)", nomination.Title, err, response)
		}
	}
	return nil
}

func (ns NominationsService) applyLabel(nomination awards.Nomination, label string) error {
	var response *gitlab.Response
	var err error
	if nomination.WebURL.IsAnIssue() {
		_, response, err = ns.client.Issues.UpdateIssue(nomination.WebURL.ProjectPath(), nomination.WebURL.ID(), &gitlab.UpdateIssueOptions{AddLabels: &gitlab.Labels{label}})
	} else {
		_, response, err = ns.client.MergeRequests.UpdateMergeRequest(nomination.WebURL.ProjectPath(), nomination.WebURL.ID(), &gitlab.UpdateMergeRequestOptions{AddLabels: &gitlab.Labels{label}})
	}
	if err != nil {
		return fmt.Errorf("Error while applying label %q on %q: %s (response: %+v)", label, nomination.WebURL, err, response)
	}
	return nil
}

func (ns NominationsService) removeLabel(nomination awards.Nomination, label string) error {
	var response *gitlab.Response
	var err error
	if nomination.WebURL.IsAnIssue() {
		_, response, err = ns.client.Issues.UpdateIssue(nomination.WebURL.ProjectPath(), nomination.WebURL.ID(), &gitlab.UpdateIssueOptions{RemoveLabels: &gitlab.Labels{label}})
	} else {
		_, response, err = ns.client.MergeRequests.UpdateMergeRequest(nomination.WebURL.ProjectPath(), nomination.WebURL.ID(), &gitlab.UpdateMergeRequestOptions{RemoveLabels: &gitlab.Labels{label}})
	}
	if err != nil {
		return fmt.Errorf("Error while removing label %q on %q: %s (response: %+v)", label, nomination.WebURL, err, response)
	}
	return nil
}
