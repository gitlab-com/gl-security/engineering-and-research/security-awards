package gitlab

import (
	"fmt"
	"net/http"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
)

func TestCouncilIssuesService(t *testing.T) {

	mux, ts := testServer(t)
	defer ts.Close()

	// Create teams.yml temp file
	tmpTeamFile := createTmpTeamDataFile(t)
	defer os.Remove(tmpTeamFile.Name()) // clean up

	client := TokenClient{}
	if err := client.SetCredentials("asdf123", ts.URL); err != nil {
		t.Fatal(err)
	}
	if err := client.LoadTeam(tmpTeamFile.Name()); err != nil {
		t.Fatal(err)
	}

	issue := &awards.CouncilIssue{
		Title:        "Security-Awards Council Discussion week of 2021-25-01",
		WebURL:       "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1",
		IID:          1,
		Confidential: true,
	}

	// JSON response from the GitLab API for a council issue
	jsonIssue := `
{
   "assignees" : [
      {
         "id" : 1
      }
   ],
   "author" : {
      "id" : 1,
      "name" : "plafoucriere"
   },
   "confidential" : true,
   "description" : "Description of the issue",
   "id" : 123,
   "iid" : 1,
   "title" : "Security-Awards Council Discussion week of 2021-25-01",
   "web_url" : "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1"
}
`
	t.Run("CreateIssue", func(t *testing.T) {
		mux.HandleFunc("/api/v4/projects/{project_path}/issues", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, jsonIssue)
		}).Methods(http.MethodPost)

		got, err := client.CouncilIssues().CreateIssue(1, "Security-Awards Council Discussion week of 2021-25-01", "Description of the issue", []string{"someLabel"})
		if err != nil {
			t.Fatal(err)
		}

		if diff := cmp.Diff(issue, got); diff != "" {
			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}

	})
	t.Run("CreateIssueDiscussion", func(t *testing.T) {
		mux.HandleFunc("/api/v4/projects/{project_path}/issues/{issue_id:[0-9]+}/discussions", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, `{"id": "1"}`)
		}).Methods(http.MethodPost)

		err := client.CouncilIssues().CreateIssueDiscussion(*issue, "Body")
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("ListIssues", func(t *testing.T) {
		mux.HandleFunc("/api/v4/projects/{project_path}/issues", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "[%s]", jsonIssue)
		}).Methods(http.MethodGet)
		issues, err := client.CouncilIssues().ListIssues(1)
		if err != nil {
			t.Fatal(err)
		}
		want := []awards.CouncilIssue{*issue}
		if diff := cmp.Diff(want, issues); diff != "" {
			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}
	})

	t.Run("ListNominations", func(t *testing.T) {
		// Default for council issues discussions
		mux.HandleFunc("/api/v4/projects/{project_path}/issues/{issue_id:[0-9]+}/discussions", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprint(w, `
[
  {
    "id": "6a9c1750b37d513a43987b574953fceb50b03ce7",
    "individual_note": false,
    "notes": [
      {
        "id": 1126,
        "type": "DiscussionNote",
        "body": "### [Sign OAuth](https://gitlab.com/gitlab-org/security/gitlab-pages/-/merge_requests/6)\n\nNominated by @dappelt",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-03T21:54:39.668Z",
        "updated_at": "2018-03-03T21:54:39.668Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Issue",
        "noteable_iid": null
      }
    ]
  },
  {
    "id": "6a9c1750b37d513a43987b574953fceb50b03ce8",
    "individual_note": false,
    "notes": [
      {
        "id": 1127,
        "type": "DiscussionNote",
        "body": "### [Awesome security improvement](https://gitlab.com/gitlab-org/security/gitaly/-/merge_requests/125)\n\nNominated by @vdesousa",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2021-03-03T21:54:39.668Z",
        "updated_at": "2021-03-03T21:54:39.668Z",
        "system": false,
        "noteable_id": 4,
        "noteable_type": "Issue",
        "noteable_iid": null
      }
    ]
  }
]
`)
		}).Methods(http.MethodGet)

		// The nomination "Awesome security improvement" receives 2 thumbsup
		mux.HandleFunc("/api/v4/projects/{project_path}/issues/123/notes/1127/award_emoji", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprint(w, `
[
  {
    "id": 1,
    "name": "thumbsup",
    "user": {
      "name": "Dennis Appelt",
      "username": "dappelt",
      "id": 1,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2016-06-15T10:09:34.206Z",
    "updated_at": "2016-06-15T10:09:34.206Z",
    "awardable_id": 80,
    "awardable_type": "Issue"
  },
  {
    "id": 2,
    "name": "thumbsup_tone1",
    "user": {
      "name": "Ron",
      "username": "rchan-gitlab",
      "id": 26,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/7e65550957227bd38fe2d7fbc6fd2f7b?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/rchan-gitlab"
    },
    "created_at": "2016-06-15T10:09:34.177Z",
    "updated_at": "2016-06-15T10:09:34.177Z",
    "awardable_id": 80,
    "awardable_type": "Issue"
  }
]
`)
		}).Methods(http.MethodGet)

		// Default award emojis on notes (only 1 thumbup)
		mux.HandleFunc("/api/v4/projects/{project_path}/issues/{issue_id:[0-9]+}/notes/{note_id:[0-9]+}/award_emoji", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprint(w, `
[
  {
    "id": 1,
    "name": "thumbsup",
    "user": {
      "name": "Ron",
      "username": "rchan-gitlab",
      "id": 26,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/7e65550957227bd38fe2d7fbc6fd2f7b?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/rchan-gitlab"
    },
    "created_at": "2016-06-15T10:09:34.177Z",
    "updated_at": "2016-06-15T10:09:34.177Z",
    "awardable_id": 80,
    "awardable_type": "Issue"
  }
]
`)
		}).Methods(http.MethodGet)

		mux.HandleFunc("/api/v4/projects/{project_path}/{resource[(issues|merge_requests)]}/{id:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprint(w, `
{
  "id": 1122334455,
  "iid": 125,
  "project_id": 3,
  "author": {
    "id": 1,
    "name": "Andy Volpe",
    "username": "andyvolpe",
    "state": "active",
    "avatar_url": null,
    "web_url": "https://gitlab.example.com/admin"
  }
}
`)
		}).Methods(http.MethodGet, http.MethodPut)

		nominations, err := client.CouncilIssues().ListNominations(*issue)
		if err != nil {
			t.Fatal(err)
		}

		want := []awards.Nomination{
			{
				Category:        "engineering",
				WebURL:          "https://gitlab.com/gitlab-org/security/gitlab-pages/-/merge_requests/6",
				Title:           "Sign OAuth",
				NominatedBy:     "dappelt",
				CouncilIssueURL: "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1#note_1126",
				ThumbsUP:        []awards.GitLabUser{"rchan-gitlab"},
				Author:          "andyvolpe",
			},
			{
				Category:        "engineering",
				WebURL:          "https://gitlab.com/gitlab-org/security/gitaly/-/merge_requests/125",
				Title:           "Awesome security improvement",
				NominatedBy:     "vdesousa",
				CouncilIssueURL: "https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1#note_1127",
				ThumbsUP:        []awards.GitLabUser{"rchan-gitlab"},
				Author:          "andyvolpe",
			},
		}

		if diff := cmp.Diff(want, nominations); diff != "" {
			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}

	})

	t.Run("CloseIssue", func(t *testing.T) {
		err := client.CouncilIssues().CloseIssue(*issue)
		if err != nil {
			t.Fatal(err)
		}
	})
}
