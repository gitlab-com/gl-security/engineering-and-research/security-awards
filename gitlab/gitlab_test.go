package gitlab

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/gorilla/mux"
)

func testServer(t *testing.T) (*mux.Router, *httptest.Server) {
	mux := mux.NewRouter().UseEncodedPath()

	// Add a middleware to check header on all requests
	mux.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// The REST and GraphQL APIs use different authentication headers
			switch {
			case strings.HasPrefix(r.URL.Path, "/api/v4/"): // REST API
				if auth := r.Header.Get("Private-Token"); auth != "asdf123" {
					t.Errorf("[%s] Authorization header: expected: asdf123, got: %s\n", r.URL, auth)
				}
			case strings.HasPrefix(r.URL.Path, "/api/graphql"): // GraphQL API
				if auth := r.Header.Get("Authorization"); auth != "Bearer asdf123" {
					t.Errorf("[%s] Authorization header: expected: Bearer asdf123, got: %s\n", r.URL, auth)
				}
			default: // Other URLs than the APIs should be an error
				t.Errorf("Unknown path: %s", r.URL.Path)
			}

			if r.Method == http.MethodPost {
				if ct := r.Header.Get("Content-Type"); ct != "application/json" {
					t.Errorf("Content-Type header: expected: application/json, got: %s\n", ct)
				}
			}
			// Call the next handler, which can be another middleware in the chain, or the final handler.
			next.ServeHTTP(w, r)
		})
	})

	return mux, httptest.NewServer(mux)
}

func createTmpTeamDataFile(t *testing.T) *os.File {
	team := `
- slug: dennis-a
  name: Dennis Appelt
  gitlab: dappelt
  departments:
    - Engineering Function
    - Security Department
- slug: ronchan
  name: Ron Chan
  gitlab: rchan-gitlab
  departments:
    - Engineering Function
    - Security Department
- slug: andy-volpe
  name: Andy Volpe
  gitlab: andyvolpe
  departments:
    - Engineering Function
    - UX Department
    - Secure UX Team
- slug: robert-may
  name: Robert May
  gitlab: robotmay_gitlab
  departments:
  - Engineering Function
  - Development Department
  - Dev Section
  - Create:Source Code BE Team
- slug: heinrich-lee-yu
  name: Heinrich Lee Yu
  gitlab: engwan
  departments:
  - Engineering Function
  - Development Department
  - Dev Section
  - Plan:Project Management BE Team
`

	return createTmpFile(t, "team.yml", []byte(team))
}

func createTmpFile(t *testing.T, filename string, data []byte) *os.File {
	tmpfile, err := ioutil.TempFile("", filename)
	if err != nil {
		t.Fatal(err)
	}

	if _, err := tmpfile.Write(data); err != nil {
		t.Fatal(err)
	}

	if err := tmpfile.Close(); err != nil {
		t.Fatal(err)
	}

	return tmpfile
}
