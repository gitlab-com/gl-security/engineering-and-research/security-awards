package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/gitlab"
)

const (
	defaultDataFile   = "./data/awards.yml"
	defaultTeamFile   = "./data/team.yml"
	defaultPrizesFile = "./data/prizes.yml"

	// APITokenFlagName is the name of the flag "api-token"
	APITokenFlagName = "api-token"
	// APIBaseURLFlagName is the name of the flag "api-baseurl"
	APIBaseURLFlagName = "api-baseurl"

	dryRunFlagName           = "dry-run"
	councilProjectIDFlagName = "project-id"
	dontSendCongratsFlagName = "no-congrats"
	dataFileFlagName         = "data-file"
)

func main() {
	app := NewApp(&gitlab.TokenClient{})
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal("Error:", err)
	}
}

var dataFileFlag = &cli.StringFlag{
	Name:    dataFileFlagName,
	Aliases: []string{"f"},
	Value:   defaultDataFile,
	EnvVars: []string{"AWARDS_FILE_PATH"},
}

var teamFileFlag = &cli.StringFlag{
	Name:    "team-file",
	Value:   defaultTeamFile,
	EnvVars: []string{"TEAM_FILE_PATH"},
}

var prizesFileFlag = &cli.StringFlag{
	Name:    "prizes-file",
	Value:   defaultPrizesFile,
	EnvVars: []string{"PRIZES_FILE_PATH"},
}

var gitlabAPIFlags = []cli.Flag{
	&cli.StringFlag{
		Name:    APITokenFlagName,
		Aliases: []string{"t"},
		EnvVars: []string{"GITLAB_API_TOKEN"},
	},
	&cli.StringFlag{
		Name:    APIBaseURLFlagName,
		Value:   "https://gitlab.com/",
		EnvVars: []string{"GITLAB_API_BASEURL"},
	},
}

var noColorFlag = &cli.BoolFlag{
	Name:    "no-color",
	EnvVars: []string{"NO_COLOR"},
}

var councilProjectIDFlag = &cli.IntFlag{
	Name:    councilProjectIDFlagName,
	Value:   4501968, // https://gitlab.com/gitlab-com/gl-security/security-department-meta/
	EnvVars: []string{"COUNCIL_PROJECT_ID"},
}

// NewApp returns a *cli.App with all commands and flags configured
func NewApp(git gitlab.Client) *cli.App {
	app := cli.NewApp()
	app.Commands = []*cli.Command{
		{
			Name:    "awards",
			Aliases: []string{"a"},
			Usage:   "Awards resources",
			Subcommands: []*cli.Command{
				{
					Name:  "validate",
					Usage: "Validate awards YAML file and data",
					Flags: []cli.Flag{
						noColorFlag,
						dataFileFlag,
					},
					Action: validateCmd,
				},
			},
		},
		{
			Name:    "council-issues",
			Aliases: []string{"c"},
			Usage:   "Council Issues resources",
			Subcommands: []*cli.Command{
				{
					Name:  "create",
					Usage: "Create a council issue for new nominations",
					Flags: append(gitlabAPIFlags, []cli.Flag{
						councilProjectIDFlag,
						&cli.BoolFlag{
							Name:    dryRunFlagName,
							Aliases: []string{"n"},
							Usage:   "Run without creating the issue (no hit on the API)",
						},
					}...),
					Before: initClient(git),
					Action: createCouncilIssueCmd(git),
				},
				{
					Name:  "close",
					Usage: "Close the open council issue(s)",
					Flags: append(gitlabAPIFlags, []cli.Flag{
						dataFileFlag,
						teamFileFlag,
						councilProjectIDFlag,
						&cli.BoolFlag{
							Name:    dontSendCongratsFlagName,
							Usage:   "Disable commenting on awarded issues or MRs",
							EnvVars: []string{"NO_AWARDS_CONGRATS"},
						},
					}...),
					Before: initClient(git),
					Action: closeCouncilIssueCmd(git),
				},
			},
		},
		{
			Name:    "leaderboard",
			Aliases: []string{"l"},
			Usage:   "Leaderboard resource",
			Subcommands: []*cli.Command{
				{
					Name:  "generate",
					Usage: "Generate leaderboard page from awards data file",
					Flags: []cli.Flag{
						dataFileFlag,
					},
					Action: generateLeaderboardCmd,
				},
			},
		},
		{
			Name:    "nominations",
			Aliases: []string{"n"},
			Usage:   "Nominations resources",
			Subcommands: []*cli.Command{
				{
					Name:  "list",
					Usage: "List current nominations",
					Flags: append(gitlabAPIFlags, []cli.Flag{
						noColorFlag,
						&cli.StringFlag{
							Name:    "output",
							Aliases: []string{"o"},
							Value:   "txt",
							Usage:   "Output format: txt (default), json, yaml",
						},
					}...),
					Before: initClient(git),
					Action: fetchCmd(git),
				},
			},
		},
		{
			Name:    "prizes",
			Aliases: []string{"p"},
			Usage:   "Prizes resource",
			Subcommands: []*cli.Command{
				{
					Name:  "generate",
					Usage: "Generate prizes page from prizes data file",
					Flags: []cli.Flag{
						prizesFileFlag,
					},
					Action: generatePrizesCmd,
				},
			},
		},
		{
			Name:    "security-merge-requests",
			Aliases: []string{"smr"},
			Usage:   "Security Merge Requests",
			Subcommands: []*cli.Command{
				{
					Name:  "list",
					Usage: "List Security Merge Requests",
					Flags: append(gitlabAPIFlags, []cli.Flag{
						dataFileFlag,
						teamFileFlag,
					}...),
					Before: initClient(git),
					Action: listSecurityMRsCmd(git),
				},
				{
					Name:  "reward",
					Usage: "Reward Security Merge Requests",
					Flags: append(gitlabAPIFlags, []cli.Flag{
						dataFileFlag,
						teamFileFlag,
					}...),
					Before: initClient(git),
					Action: rewardSecurityMRsCmd(git),
				},
			},
		},
	}
	return app
}

func initClient(git gitlab.Client) func(*cli.Context) error {
	return func(c *cli.Context) error {
		if err := git.SetCredentials(c.String(APITokenFlagName), c.String(APIBaseURLFlagName)); err != nil {
			return err
		}
		if c.String("team-file") != "" {
			if err := git.LoadTeam(c.String("team-file")); err != nil {
				return err
			}
		}
		return nil
	}
}
