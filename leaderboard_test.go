package main

import (
	"bytes"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestLeaderboard(t *testing.T) {
	t.Run("create", func(t *testing.T) {

		tmpfile := createTmpAwardsDataFile(t)
		defer os.Remove(tmpfile.Name()) // clean up

		output := new(bytes.Buffer)
		app := NewApp(&TestClient{})
		app.Writer = output
		err := app.Run([]string{os.Args[0], "leaderboard", "generate", "-f", tmpfile.Name()})
		if err != nil {
			t.Error(err)
		}
		want := `---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY21

## Yearly

No yearly board for FY21.

## FY21-Q4

### Development

Category is empty

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@pks-t](https://gitlab.com/pks-t) | 1 | 200 |

### Non-Engineering

Category is empty

### Community

Category is empty

## FY21-Q3

### Development

Category is empty

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 1 | 200 |

### Non-Engineering

Category is empty

### Community

Category is empty


`
		if diff := cmp.Diff(want, output.String()); diff != "" {
			t.Fatalf("validate output mismatch (-want +got): %s", diff)
		}

	})
}
