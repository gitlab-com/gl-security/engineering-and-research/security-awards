# Security Awards CLI

This repository hosts the CLI for the [Security Awards](https://about.gitlab.com/handbook/security/security-awards-program.html) Program.

## Usage

The easiest way to run the cli tool is to use the docker image provided in this repo:

```sh
alias awards="docker run -it --rm -v $PWD/data:/data/ registry.gitlab.com/gitlab-com/security-awards"
```

or if you don't want to use `docker`:

```sh
go build -o awards
```

### Close council issues

The [council issues](https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues?label_name[]=Security%20Awards%20Council)
are used to vote for nominations. They must be closed every week to award nominees.


```
 export GITLAB_API_TOKEN="<api token>"
awards council-issues close
```

WARNING: This will close the opened council issues, and update related issues and merge requests (nominations).

### Create a new council issue


```
 export GITLAB_API_TOKEN="<api token>"
awards council-issues create
```

### Generate the leaderboard page

```
awards leaderboard generate
```

This will generate a markdown page with a leaderboard corresponding to the awards data in the `data` directory.

### Generate the prizes page

```
awards prizes generate
```
