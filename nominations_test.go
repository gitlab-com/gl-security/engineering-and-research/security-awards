package main

import (
	"bytes"
	"fmt"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestList(t *testing.T) {
	t.Run("Issues and MRs", func(t *testing.T) {
		os.Setenv("GITLAB_API_TOKEN", "asdf123")

		output := new(bytes.Buffer)
		app := NewApp(&TestClient{})
		app.Writer = output

		err := app.Run([]string{os.Args[0], "nominations", "list", "--no-color"})
		if err != nil {
			t.Fatal(err)
		}
		got := output.String()

		want :=
			`[New]

  * A non-member can merge an MR to master - https://gitlab.com/gitlab-org/gitlab/-/issues/290710 [@rchan-gitlab]
  * Forbid public - https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1117 [@rchan-gitlab]
  * Fix issues - https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1095 [@rchan-gitlab]
  * Replace umask usage with files permission change when a non-root image used - https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2539 [@rchan-gitlab]
  * Add encrypted ldap secrets support - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45712 [@rchan-gitlab]

[https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/123]

  * Sign OAuth - https://gitlab.com/gitlab-org/security/gitlab-pages/-/merge_requests/6 [@rchan-gitlab]

`
		if diff := cmp.Diff(want, got); diff != "" {
			// full output
			fmt.Printf("got = %+v\n", got)

			t.Errorf("List output mismatch (-want +got):\n%s", diff)
		}
	})
}
