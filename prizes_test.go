package main

import (
	"bytes"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestPrizes(t *testing.T) {
	t.Run("generate", func(t *testing.T) {

		tmpfile := createTmpPrizesDataFile(t)
		defer os.Remove(tmpfile.Name()) // clean up

		output := new(bytes.Buffer)
		app := NewApp(&TestClient{})
		app.Writer = output
		err := app.Run([]string{os.Args[0], "prizes", "generate", "--prizes-file", tmpfile.Name()})
		if err != nil {
			t.Error(err)
		}
		want := `---
layout: handbook-page-toc
title: "Security Awards Prizes"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Prizes FY22

## Yearly

 | Category  | Rank   | Prize                          |
 | --------  | ---:   | -----                          |
 | All       | 1      | $1000 Amazon Gift Card         |
 | All       | 2-250  | GitLab Security Ninja Sticker  |

## FY22-Q2

 | Category         | Rank   | Prize                  |
 | --------         | ---:   | -----                  |
 | Development      | 1-10   | $100 Amazon Gift Card  |
 | Development      | 11-25  | $20 gitlab swag shop   |
 | Engineering      | 1-3    | $100 Amazon Gift Card  |
 | Non-Engineering  | 1-3    | $100 Amazon Gift Card  |
 | Community        | 1      | $100 Amazon Gift Card  |

## FY22-Q1

 | Category         | Rank   | Prize                  |
 | --------         | ---:   | -----                  |
 | Development      | 1-10   | $100 Amazon Gift Card  |
 | Development      | 11-25  | $20 gitlab swag shop   |
 | Engineering      | 1-3    | $100 Amazon Gift Card  |
 | Non-Engineering  | 1-3    | $100 Amazon Gift Card  |
 | Community        | 1      | $100 Amazon Gift Card  |

# Prizes FY21

## FY21-Q4

 | Category         | Rank  | Prize                  |
 | --------         | ---:  | -----                  |
 | Development      | 1-5   | $100 Amazon Gift Card  |
 | Development      | 6-25  | $20 gitlab swag shop   |
 | Engineering      | 1-5   | $20 gitlab swag shop   |
 | Non-Engineering  | 1-5   | $20 gitlab swag shop   |
 | Community        | 1     | $100 Amazon Gift Card  |

`

		if diff := cmp.Diff(want, output.String()); diff != "" {
			t.Fatalf("validate output mismatch (-want +got): %s", diff)
		}

	})
}
