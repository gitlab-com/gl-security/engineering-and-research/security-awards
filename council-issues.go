package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"strings"
	"text/template"
	"time"

	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/awards"
	"gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/gitlab"
	"sigs.k8s.io/yaml"
)

func createCouncilIssueCmd(git gitlab.Client) func(*cli.Context) error {
	return func(c *cli.Context) error {

		var councilIssueNominationThreadTpl = template.Must(template.New("nomination").Parse(councilIssueNominationThread))
		dryRun := c.Bool(dryRunFlagName)
		pid := c.Int(councilProjectIDFlagName)

		// TODO: Check if council issue already exist for the current week

		fmt.Fprintf(c.App.Writer, "Gathering nominations, please wait...")
		nominations, err := git.Nominations().GetNominations()
		if err != nil {
			return err
		}
		fmt.Fprintf(c.App.Writer, " [OK]")

		group := nominations.GroupByCouncilIssues()

		newNominations := group["New"]

		lastMonday := lastMonday()
		title := fmt.Sprintf(councilIssueTitle, lastMonday.Format(awards.LayoutISO))

		labels := []string{"Security Awards Council"}

		fmt.Fprintln(c.App.Writer, "\nGenerating issue with:")
		fmt.Fprintln(c.App.Writer, "\t[Project ID]:", pid)
		fmt.Fprintln(c.App.Writer, "\t[Title]:", title)
		fmt.Fprintln(c.App.Writer, "\t[Confidential]:", true)
		fmt.Fprintln(c.App.Writer, "\t[Labels]:", labels)

		var issue *awards.CouncilIssue

		if !dryRun {
			issue, err = git.CouncilIssues().CreateIssue(pid, title, councilIssueDescriptionTpl, labels)
			if err != nil {
				return err
			}
			fmt.Fprintln(c.App.Writer, "\t[URL]:", issue.WebURL)
		}

		for i, nomination := range newNominations {
			var b bytes.Buffer
			err := councilIssueNominationThreadTpl.Execute(&b, nomination)
			if err != nil {
				return err
			}
			fmt.Fprintf(c.App.Writer, "\t[Discussion %d]:", i+1)
			fmt.Fprintf(c.App.Writer, "%v", indent(b.String(), "\t\t"))
			if !dryRun {
				err := git.CouncilIssues().CreateIssueDiscussion(*issue, b.String())
				if err != nil {
					return err
				}
			}
		}

		fmt.Fprintf(c.App.Writer, "Done (%d discussions)\n", len(newNominations))
		return nil
	}
}

// lastMonday returns the date of the last Monday in the current week
func lastMonday() time.Time {
	date := time.Now()
	for date.Weekday() != time.Monday {
		date = date.AddDate(0, 0, -1)
	}
	return date
}

func indent(text, indent string) string {
	if text[len(text)-1:] == "\n" {
		result := ""
		for _, j := range strings.Split(text[:len(text)-1], "\n") {
			result += indent + j + "\n"
		}
		return result
	}
	result := ""
	for _, j := range strings.Split(strings.TrimRight(text, "\n"), "\n") {
		result += indent + j + "\n"
	}
	return result[:len(result)-1]
}

// PointsPerVote is the number of points to awards per votes on nominations (thumbsup)
const PointsPerVote = 100

const awardCongratulationsMsg = `Congratulations :tada: %s, your Issue/Merge Request has been awarded! ([Learn more about the Security Awards Program](https://about.gitlab.com/handbook/security/security-awards-program.html))`

func closeCouncilIssueCmd(git gitlab.Client) func(*cli.Context) error {
	return func(c *cli.Context) error {
		// Load awards
		// Fetch open Council Issues
		// For each Council Issue:
		//   Fetch Nominations from discussions
		//   For each nomination:
		//     if votes > 2:
		//       Generate Award
		//       Append award to awards
		//       Label nomination as ~security-awards::awarded
		//       Send Congrats
		//     else
		//       Remove ~security-awards::awarded label from nomination
		//   Close issue
		// Save awards to disk

		// Load awards
		awardsList, err := awards.LoadAwards(c.String(dataFileFlagName))
		if err != nil {
			return err
		}

		// If awardsSize is the same at the end, no need to write the file again
		awardsInitialSize := len(awardsList)

		pid := c.Int(councilProjectIDFlagName)

		// find open Council Issues
		issues, err := git.CouncilIssues().ListIssues(pid)
		if err != nil {
			return err
		}

		if len(issues) == 0 {
			fmt.Fprintln(c.App.Writer, "No open council issue found. Nothing to do!")
			return nil
		}

		fmt.Fprintf(c.App.Writer, "Found %d open council issue(s).\n", len(issues))

		// For each Council Issue:
		for _, issue := range issues {

			// Fetch Nominations from discussions
			nominations, err := git.CouncilIssues().ListNominations(issue)
			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "\n[Council Issue] %s\n", issue.Title)

			for i := range nominations {
				// avoid memory aliasing in loop (https://cwe.mitre.org/data/definitions/118.html)
				nomination := &nominations[i]

				fmt.Fprintf(c.App.Writer, "Checking nomination for %q: ", nomination.Title)

				if len(nomination.ThumbsUP) < 2 {
					fmt.Fprintf(c.App.Writer, "Not enough votes ")
					err := git.Nominations().RejectNomination(*nomination)
					if err != nil {
						return err
					}
					fmt.Fprint(c.App.Writer, "[Rejected]\n")
					continue
				}

				nomination.Points = uint(len(nomination.ThumbsUP)) * PointsPerVote

				// Generate new Award and add it to the list
				awardsList = append(awardsList, awards.Award{
					Date:       now(),
					Nomination: nomination,
				})

				// Label nomination as ~security-awards::awarded
				congrats := ""
				if !c.Bool(dontSendCongratsFlagName) {
					congrats = fmt.Sprintf(awardCongratulationsMsg, nomination.Author)
				}
				err = git.Nominations().AwardNomination(*nomination, congrats)
				if err != nil {
					return err
				}
				fmt.Fprint(c.App.Writer, "[Awarded]\n")
			}

			// Close issue
			err = git.CouncilIssues().CloseIssue(issue)
			if err != nil {
				return err
			}
		}

		if len(awardsList) == awardsInitialSize {
			fmt.Fprintln(c.App.Writer, "No new awards to save to disk.")
			return nil
		}

		awardsData, err := yaml.Marshal(awardsList)
		if err != nil {
			return err
		}

		fmt.Fprintf(c.App.Writer, "\nWriting data file (%s): ", c.String(dataFileFlagName))
		err = ioutil.WriteFile(c.String(dataFileFlagName), awardsData, 0600)
		if err != nil {
			return err
		}
		fmt.Fprint(c.App.Writer, "[OK]\n")

		return nil
	}
}

const (
	councilIssueTitle = `Security-Awards Council Discussion week of %s`

	councilIssueDescriptionTpl = `This issue is to vote on nominated actions, in the context of the [Security Awards Program](https://about.gitlab.com/handbook/security/security-awards-program.html).

### How to vote?

Approvals are indicated by :+1: from 2 team members.  
Criteria to approve a nomination: you agree that the action awarded improves the security of GitLab in some way.

The number of votes is the number of :+1:, there's no limit on the number of votes you can use.

/cc @gitlab-com/gl-security/appsec @gitlab-com/gl-security/secops @gitlab-com/gl-security/security-assurance

_This issue was created automatically by the Security Awards Program [cli](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-awards/)._
`

	councilIssueNominationThread = `
### [{{.Title}}]({{.WebURL}})

Nominated by {{.NominatedBy}}
`
)

func now() awards.JSONDate {
	d := awards.JSONDate{}
	now := time.Now()
	d.Time = &now
	return d
}
