package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestAwards(t *testing.T) {
	t.Run("validate", func(t *testing.T) {

		tmpfile := createTmpAwardsDataFile(t)
		defer os.Remove(tmpfile.Name()) // clean up

		output := new(bytes.Buffer)
		app := NewApp(&TestClient{})
		app.Writer = output
		err := app.Run([]string{os.Args[0], "awards", "validate", "-f", tmpfile.Name(), "--no-color"})
		if err != nil {
			t.Error(err)
		}
		want := "[OK] Data file %s is valid\n"

		if diff := cmp.Diff(fmt.Sprintf(want, tmpfile.Name()), output.String()); diff != "" {
			t.Fatalf("validate output mismatch (-want +got): %s", diff)
		}

	})
}

func createTmpAwardsDataFile(t *testing.T) *os.File {
	awardsYAML := `- date: "2020-11-23"
  nomination:
    author: pks-t
    category: engineering
    council_issue_url: https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1089#note_447389715
    nominated_by: rchan-gitlab
    points: 200
    thumbs_up:
    - dappelt
    - plafoucriere
    web_url: https://gitlab.com/gitlab-org/gitaly/-/merge_requests/2669
- date: "2020-08-23"
  nomination:
    author: igor.drozdov
    category: engineering
    council_issue_url: https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1090#note_447389716
    nominated_by: dappelt
    points: 200
    thumbs_up:
    - rchan-gitlab
    - plafoucriere
    web_url: https://gitlab.com/gitlab-org/gitaly/-/merge_requests/2670
`
	return createTmpFile(t, "awards.yml", []byte(awardsYAML))
}

func createTmpPrizesDataFile(t *testing.T) *os.File {
	prizesYAML := `amazon_giftcard_USD_100: &amazon-100
  name: $100 Amazon Gift Card
  value: 100
  currency: usd

amazon_giftcard_USD_1000: &amazon-1000
  name: $1000 Amazon Gift Card
  value: 1000
  currency: usd

gitlab_giftcard_USD_20: &gitlab-20
  name: $20 gitlab swag shop
  value: 20
  currency: usd

gitlab_security_ninja_sticker: &gitlab-ninja-sticker
  name: GitLab Security Ninja Sticker
  value: 0.27
  currency: usd

prizes:
  - year: fy22
    Yearly:
      - rank_end: 1
        prize:
          <<: *amazon-1000
      - rank_end: 250
        prize:
          <<: *gitlab-ninja-sticker
    Q2:
      - category: dev
        rank_end: 10
        prize:
          <<: *amazon-100
      - category: dev
        rank_end: 25
        prize:
          <<: *gitlab-20
      - category: engineering
        rank_end: 3
        prize:
          <<: *amazon-100
      - category: non-engineering
        rank_end: 3
        prize:
          <<: *amazon-100
      - category: community
        rank_end: 1
        prize:
          <<: *amazon-100
    Q1:
      - category: dev
        rank_end: 10
        prize:
          <<: *amazon-100
      - category: dev
        rank_end: 25
        prize:
          <<: *gitlab-20
      - category: engineering
        rank_end: 3
        prize:
          <<: *amazon-100
      - category: non-engineering
        rank_end: 3
        prize:
          <<: *amazon-100
      - category: community
        rank_end: 1
        prize:
          <<: *amazon-100
  - year: fy21
    Q4:
      - category: dev
        rank_end: 5
        prize:
          <<: *amazon-100
      - category: dev
        rank_end: 25
        prize:
          <<: *gitlab-20
      - category: engineering
        rank_end: 5
        prize:
          <<: *gitlab-20
      - category: non-engineering
        rank_end: 5
        prize:
          <<: *gitlab-20
      - category: community
        rank_end: 1
        prize:
          <<: *amazon-100
`
	return createTmpFile(t, "prizes.yml", []byte(prizesYAML))
}

func createTmpFile(t *testing.T, filename string, data []byte) *os.File {
	tmpfile, err := ioutil.TempFile("", filename)
	if err != nil {
		t.Fatal(err)
	}

	if _, err := tmpfile.Write(data); err != nil {
		t.Fatal(err)
	}

	if err := tmpfile.Close(); err != nil {
		t.Fatal(err)
	}

	return tmpfile
}
